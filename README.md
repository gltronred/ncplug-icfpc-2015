NCPLUG ICFPC 2015 entry
=======================

```
"Russian computer scientists in the 1960s discovered a way to visually encode spells..."
```

Installation and launch
-----------------------

Our solution is located at `http://bitbucket.org/gltronred/ncplug-icfpc-2015`. After end of contest we will make it public

Just build with stack:

```
stack build --flag ncplug-icfpc:production --flag solution:production
```

And run solution (NOTE: it can output to stdout and stderr, even without -d):

```
stack exec solution -- -f problems/problem_21.json -s one -S print -d DEBUG -c 5

stack exec solution -- -f d:\Projects\ICFPC2015\ncplug-icfpc-2015\problems\problem_0.json -p "ei!" -p "yuggoth" -p "r'lyeh" -p "ph'
nglui mglw'nafh cthulhu r'lyeh wgah'nagl fhtagn!" -p "necronomicon" -p "planet 10" -p "monkeyboy" -p "john bigboote" -s trajectoryWP -S print -d DEBUG -c 5
```

Additional flags:

`-s` :: solver to use. Available solvers:

- trajectoryWP (default)
- trajectory
- test (sends "ei!")
- manual (play manually)
- one (plays on boards with one cell only)
- oneEx (more complicated one)
- random (random movements)
- unitAStar (try to put piece with A*)

`-S` :: submitter to use. Available:

- submit (default)
- print (just shows on screen)

`-d` :: debug level. If not set, no output should be written. Available:

- DEBUG
- NOTICE
- INFO
- WARNING
- ERROR
- CRITICAL
- ALERT
- EMERGENCY
  
`-h`, `--help` :: shows (some) help

Search for phrases
------------------

Send phrases:

```
stack exec send-phrase problems/problem_2.json "mayor" "president" "turing" "machine" "turing machine" "danger" "qualifier" "postmortem" "iukkoth" "lovecraft" "miscatonic" "miscatonic university" "k'n yan" "k'n-yan" "xinaian" "arkham"
```

and look for power:

```
curl --user :Su/0q3BLeTtiZDoYsG9QmrG9oakp7L6iOC9VvR3auws= -X GET -H "Content-Type: application/json" https://davar.icfpcontest.org/teams/48/solutions| json_pp | grep -C10 '09T08:5' | most
```

Our team
--------

- Dmitry Vyal aka akamaus
- Alexey Pirogov aka astynax
- Alexander Tchitchigin aka Gabriel
- Mansur Ziiatdinov aka red aka gltronred
- Sergey Mironov aka grwlf
- Ivan Bazanov aka YanTayga

- izubkov@gmail.com
- walsk@yandex.ru

