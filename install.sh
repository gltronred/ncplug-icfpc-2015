#!/bin/bash -xe

# install stack
# sudo apt-get install stack -y

# install our program
stack install --flag ncplug-icfpc:production --flag solution:production

# rename it to play_icfp2015
DIR=$(stack path | grep local-install-root | sed 's/.*: //')
mv $DIR/bin/solution ./play_icfp2015


