#!/bin/sh

if [ -z $1 ]
then
	wget -O rankings.js https://davar.icfpcontest.org/rankings.js
fi

WORDS="setting
ranking
NCPLUG"

sed 's/{/{\n/g; s/}/}\n/g;' rankings.js | grep -F "$WORDS" | sed '/settings/ d; /setting/ s/,.*$//g; s/,"tags":.*$//g;'

