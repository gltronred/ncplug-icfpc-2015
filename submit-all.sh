#!/bin/sh

for f in problems/*; do
  stack exec solution -- -f $f -t 1200 -c 2 -p "ei!" -p "yuggoth" -p "r'lyeh" -p "ph'nglui mglw'nafh cthulhu r'lyeh wgah'nagl fhtagn!" -p "necronomicon" -p "planet 10" -p "monkeyboy" -p "john bigboote" -s trajectoryWP -S submit
  echo "$f is submitted."
done
