#!/bin/bash

# usage:
# echo ' [ { "problemId": 1, "seed": 2, "tag": "zzz", "solution": "Ei!" } ] ' | ./submit.sh 

TEAM_ID=48

RES=`curl --user :Su/0q3BLeTtiZDoYsG9QmrG9oakp7L6iOC9VvR3auws= -X POST -H "Content-Type: application/json" \
        -d "@-" \
        https://davar.icfpcontest.org/teams/$TEAM_ID/solutions`

if [ "$RES" != "created" ] ; then
    exit 1
fi
