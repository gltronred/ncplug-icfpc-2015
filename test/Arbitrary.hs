module Arbitrary where

import Lib.Types
import Lib.Board
import Lib.BoardQuery

import Control.Monad
import Control.Applicative
import Test.QuickCheck
import Test.QuickCheck.Instances

import Data.Set hiding (map)
import qualified Data.Set as Set

instance Arbitrary Cell where
  arbitrary = Cell <$> (arbitrary`suchThat`(>=0)) <*> (arbitrary`suchThat`(>=0))

instance Arbitrary Unit where
  arbitrary = Unit <$> arbitrary <*> arbitrary

instance Arbitrary GameUnit where
  arbitrary = GameUnit <$> arbitrary <*> arbitrary <*> (arbitrary`suchThat`(>=0)`suchThat`(<=5))

instance Arbitrary Board where
  arbitrary = do
    w <- (arbitrary`suchThat`(>0))
    h <- (arbitrary`suchThat`(>0))
    let b = Board w h (Set.fromList [])
    bf <- do
        n <- elements [0..w*h]
        l <- forM [0..n-1] $ \i -> do
            arbitrary`suchThat`(cellWithinBoard b)
        return $ Set.fromList (l :: [Cell])

    return b { board_filled = bf }
