module RNG_Tests where

import Test.Tasty
import Test.Tasty.HUnit

import Lib.RNG as RNG

iterate2 fun seed = let (seed',res) = fun seed
                    in res : iterate2 fun seed'

rngTests = testGroup "RNG tests"
           [ testCase "RNG outputs correct result from spec" $
             take 10 (iterate2 RNG.next 17) @?= [0,24107,16552,12125,9427,13152,21440,3383,6873,16117]
           ]

