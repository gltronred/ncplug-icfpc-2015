module IO_Tests where

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib.Types
import Lib.IO


encode_commandsTests = testGroup "encode_commands tests"
  [ testCase "encode_commands [] dummyCmds == \"bapl\"" $ encode_commands [] dummyCmds @?= "bapl"
  , testCase "encode_commands [ei] eiCmds  == \"ei!\"" $ encode_commands [ei] eiCmds @?= "ei!"
  , testCase "encode_commands [ei] ei2Cmds  == \"ei!ei!lei!ei!l\"" $ encode_commands [ei] ei2Cmds @?= "ei!ei!lei!ei!l"
  , testCase "encode_commands [ei, testPoP] ei2Cmds  == \"ei!ei!lei!ei!l\"" $ encode_commands [ei, testPoP] ei2Cmds @?= "ei!ei!lei!ei!l"
  ]
  where
    mkc = map (\cc -> Command cc Nothing)
    dummyCmds = mkc [MoveE, MoveSW, MoveW, MoveSE]
    ei = "ei!"
    eiCmds = mkc [MoveE, MoveSW, MoveW]
    ei2Cmds = eiCmds ++ dummyCmds ++ eiCmds ++ dummyCmds
    testPoP = "myi3yi"
    testPoPCmds = mkc [MoveSE, MoveE, MoveSW, MoveW, MoveE, MoveSW]
