
import Test.Tasty
import Test.Tasty.HUnit

import RNG_Tests
import Board_Tests
import IO_Tests

tests = testGroup "Tests"
        [ rngTests
        , cellTests
        , boardTests
        , moveTests
        , lowestReachableCellTests
        , encode_commandsTests
        ]


main :: IO ()
main = defaultMain tests


