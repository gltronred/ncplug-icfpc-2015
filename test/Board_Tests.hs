module Board_Tests where

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib.Types
import Lib.Board
import Lib.BoardQuery
import Lib.Astar

import Arbitrary

import Data.Set as Set
import Data.Maybe

testUnit = GameUnit [(Cell 1 1), (Cell 2 2), (Cell 3 2)] (Cell 2 2) 0
nullUnit = GameUnit [(Cell 0 0)] (Cell 0 0) 0

rotCW = moveUnit RotCW
rotCCW = moveUnit RotCCW

movSE = moveUnit MoveSE
movSW = moveUnit MoveSW

--testUnit2 = movSW $ GameUnit [(Cell 1 1), (Cell 2 2), (Cell 3 2)] (Cell 2 2)
testUnit2 = movSW $ GameUnit [(Cell 1 1)] (Cell 2 2) 0
testUnit3 = movSE $ GameUnit [(Cell 1 1), (Cell 2 2), (Cell 3 2)] (Cell 2 2) 0

testC1 = Cell 3 2
testC2 = Cell 3 (-2)
testC3 = Cell (-4) (-2)

cellTests = testGroup "Cell tests"
  [ testCase "to 3d and back1" $ (cellFrom3d . cellTo3d) testC1 @?= testC1
  , testCase "to 3d and back2" $ (cellFrom3d . cellTo3d) testC2 @?= testC2
  , testCase "to 3d and back3" $ (cellFrom3d . cellTo3d) testC3 @?= testC3
  ]

moveTests = testGroup "Move tests"
  [
    testCase "Move west" $ moveUnit MoveW testUnit @?= GameUnit [(Cell 0 1), (Cell 1 2), (Cell 2 2)] (Cell 1 2) 0
  , testCase "Move east" $ moveUnit MoveE testUnit @?= GameUnit [(Cell 2 1), (Cell 3 2), (Cell 4 2)] (Cell 3 2) 0
  , testCase "Move s west" $ moveUnit MoveSW testUnit @?= GameUnit [(Cell 1 2), (Cell 1 3), (Cell 2 3)] (Cell 1 3) 0
  , testCase "Move s east" $ moveUnit MoveSE testUnit @?= GameUnit [(Cell 2 2), (Cell 2 3), (Cell 3 3)] (Cell 2 3) 0
  , testCase "Rotate clocwise" $ rotCW testUnit @?= GameUnit [(Cell 2 1), (Cell 2 2), (Cell 2 3)] (Cell 2 2) 1
  , testCase "Rotate clocwise * 6" $ (rotCW.rotCW.rotCW.rotCW.rotCW.rotCW) testUnit @?= testUnit
  , testCase "Rotate counterclockwise" $ rotCCW testUnit @?= GameUnit [(Cell 1 2), (Cell 2 2), (Cell 2 1)] (Cell 2 2) 5
  , testCase "Rotate counterclockwise * 6" $ (rotCCW.rotCCW.rotCCW.rotCCW.rotCCW.rotCCW) testUnit @?= testUnit
  , testCase "Rot both dirs" $ (rotCW . rotCCW) testUnit @?= testUnit
  , testCase "Rot disp sw both dirs" $ (rotCW . rotCCW) testUnit2 @?= testUnit2
  , testCase "Rot disp se both dirs" $ (rotCW . rotCCW) testUnit3 @?= testUnit3
  ]

boardTests = testGroup "Board tests"
             [ testGroup "unit"
               [ testCase "(1,0) over (2,2) == (3,0)" $ rotateCell RotCW (Cell 2 2) (Cell 1 0) @?= Cell 3 0
               , testCase "(3,0) over (2,2) == (4,2)" $ rotateCell RotCW (Cell 2 2) (Cell 3 0) @?= Cell 4 2
               , testCase "(2,4) over (2,2) == (0,3)" $ rotateCell RotCW (Cell 2 2) (Cell 2 4) @?= Cell 0 3
               ]
             , testGroup "random"
               [ testProperty "rotate 6 times = id" $ let rot = moveUnit RotCW in
                 \unit -> (rot . rot . rot . rot . rot . rot) (unit :: GameUnit) == unit
               , testProperty "rotate ccw 6 times = id" $ let rot = moveUnit RotCCW in
                 \unit -> (rot . rot . rot . rot . rot . rot) (unit :: GameUnit) == unit
               , testProperty "rotate cw.ccw = id" $ \unit -> (rotCW . rotCCW) (unit :: GameUnit) == unit
               ]

             , testGroup "Board mapping"
               [ testProperty "check filled mapping" $
                 \board -> board_filled board == reverseMap (boardMap board)
               ]
             ]

lowestReachableCellTests = testGroup "Lowest reachable tests"
  [ testCase "lowestReachableCell b  == Just (Cell {cell_x = 0, cell_y = 2})" $ lowestReachableCell b  @?= Just (Cell {cell_x = 0, cell_y = 2})
  , testCase "lowestReachableCell b1 == Just (Cell {cell_x = 0, cell_y = 1})" $ lowestReachableCell b1 @?= Just (Cell {cell_x = 0, cell_y = 1})
  , testCase "lowestReachableCell b2 == Nothing" $ lowestReachableCell b2 @?= Nothing
  ]
  where
    b  = Board 3 3 (Set.fromList [Cell 1 1, Cell 1 2, Cell 2 2])
    b1 = Board 3 3 $ Set.insert (Cell 0 2) $ board_filled b
    b2 = Board 3 3 $ Set.union (Set.fromList [Cell 0 0, Cell 1 0, Cell 2 0]) $ board_filled b

emptyBoard = Board 20 20 (Set.fromList [])

drawOnEmptyBoard x = putStrLn  $ drawBoardEx emptyBoard nullUnit x

testPath = pathAstar (Cell 3 3) emptyBoard (Cell 15 10)

drawTestPath = drawOnEmptyBoard [('*', fromJust testPath), ('!',[Cell 3 3, Cell 4 3, Cell 5 3])]

drawNeighbours = drawOnEmptyBoard [('*', Set.toList $ cellNeighbours emptyBoard c )
                                  ,('!', [c])]
    where c = Cell 4 4

