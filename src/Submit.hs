{-# LANGUAGE OverloadedStrings #-}

module Submit where

import Network.HTTP.Conduit
import Control.Monad.Trans

import Lib.Types
import Lib.JSON

import Data.Aeson
import qualified Data.ByteString as BS

testOutput = Output 1 2 "zzz" "ei!" -- " [ { \"problemId\": 1, \"seed\": 2, \"tag\": \"zzz\", \"solution\": \"Ei!\" } ] "

send :: [Output] -> IO ()
send outputs = do
  req <- parseUrl "https://davar.icfpcontest.org/teams/48/solutions"
  let req' = applyBasicAuth "" "Su/0q3BLeTtiZDoYsG9QmrG9oakp7L6iOC9VvR3auws=" req
  let req'' = req' {
        method = "POST",
        requestHeaders = ("Content-Type", "application/json") : requestHeaders req',
        requestBody = RequestBodyLBS $ encode outputs
        }
  manager <- newManager tlsManagerSettings
  res <- httpLbs req'' manager
  liftIO $ print res
