module Solver.ManuallySolve
where

import Data.Maybe
import Data.Set as Set
import Data.Char
import Data.Time.Clock
import System.IO

import Lib.Types
import Lib.Board
import Lib.Command
import Lib.IO(deserialize_command)

solveManually :: STime -> [String] -> Board -> [GameUnit] -> IO Solution
solveManually limit pws board units = do
  hSetBuffering stdin NoBuffering
  iterativeSolver limit "manual" getCommandFromKB pws board units

getCommandFromKB :: SolverCommander
getCommandFromKB _ _ ss g@(GameState b h u us ls_old sc out) = do
--  print ss
  putStrLn $ "Score: " ++ show sc
  putStrLn $ drawBoard b u
--  print out
  readCommand

readCommand = do
  ch <- getChar
  case () of
    _ | ch == '\ESC' -> do print "got <ESC>, stopping"
                           return Nothing
    _ | isUpper ch || not (isAlpha ch) -> do let cmd = deserialize_command $ toLower ch
                                             case cmd of
                                               Just _ -> return $ cmd
                                               Nothing -> readCommand
      | True -> case ch of
        'q' -> return $ Just (Command MoveW Nothing)
        'e' -> return $ Just (Command MoveE Nothing)
        'a' -> return $ Just (Command MoveSW Nothing)
        'd' -> return $ Just (Command MoveSE Nothing)
        'z' -> return $ Just (Command RotCW Nothing)
        'c' -> return $ Just (Command RotCCW Nothing)
        _   -> readCommand
