{-# LANGUAGE FlexibleContexts, ScopedTypeVariables  #-}
-- Purpose: searches through game tree
-- Author: Dmitry Vyal
module Searcher(lifterBFS, getMovesFromStream, getScoreFromStream, getEstScoreFromStream, getWorldStateFromStream, Score, EstScore, SearchStream, lifterBFSDepth, fixBest, bestFirstSearch) where

import Geometry
import WorldLike

import qualified Data.Heap as H
import Data.List
import qualified Data.IntSet as S

type Score = Int -- Precise score, for selecting the best solution
type EstScore = Int -- Estimated score, for guiding the search
type Hash = Int

-- walks the search space, generates all possible moves in best first order
bestFirstSearch :: forall s. (H.HeapItem H.FstMaxPolicy (EstScore, s)) => (s -> Hash) -> (s -> [s]) -> (s -> EstScore) -> s -> [(EstScore, s)]
bestFirstSearch hasher move_generator evaluator start_state =
      bsf S.empty (H.fromAscList [(evaluator start_state, start_state)] :: H.MaxPrioHeap EstScore s)
  where bsf visited_states solutions =
          case H.view solutions of
            Nothing -> []
            Just (best, rest) -> let
              hash = hasher $ snd best
              visited_states' = S.insert hash visited_states
              child_states = move_generator $ snd best
              child_scores = map evaluator child_states
              solutions' = foldl' (flip H.insert) rest $ zip child_scores child_states
             in case S.notMember hash visited_states of
                  True -> best : bsf visited_states' solutions' -- output new state and take children into account
                  False -> bsf visited_states rest -- discarding computation, make use of laziness

-- fixes the best result so far
-- Negative depth means infinity
fixBest :: Integer -> (s -> Score) -> [s] -> [(Score,s)]
fixBest _ _ [] = error "fixBest should not be called for empty streams"
fixBest maxDepth evaluator _solution_stream@(s:ss) = fix' 0 s (evaluator s) ss
    where
      fix' step best_state best_score (cur:rest) | maxDepth < 0 || step < maxDepth =
                 let cur_score = evaluator cur
                     step' = step + 1
                 in case cur_score >= best_score of
                     True -> (cur_score, cur) : fix' step' cur cur_score rest
                     _ -> fix' step' best_state best_score rest -- or keep steps count the same?
      fix' _ _ _ _ = []

-- A BFS with separated fitness functions for state generation (estimator) and result evaluation (evaluator) phases
heuroBFS :: Integer -> (s -> Hash) -> (s -> [s]) -> (s -> Score) -> (s -> EstScore) -> s -> [(Score, (EstScore, s))]
heuroBFS depth hasher move_generator evaluator estimator start_state = fixBest depth (evaluator . snd) stream
    where stream = bestFirstSearch hasher move_generator estimator start_state

-- Lifter specific solvers

lifterBFSDepth :: (WorldLikeHashable wl, WorldLikeEng wl) =>
  Integer -> ((Path, (wl, MoveResult)) -> EstScore) -> wl -> SearchStream wl
lifterBFSDepth depth estimator w = heuroBFS depth hasher generator (scores . snd) estimator init_state
    where generator (rev_path, (world, result)) = case result of
            ContinueGame -> map (\m -> (m:rev_path, makeFullMove world m)) $ select_good_moves world all_moves
            _ -> []
          hasher (_rev_path, (world, result)) = hashWorld (world, result)
          init_state = ([], (w, ContinueGame))

-- Rejecting stupid moves when generating them
select_good_moves :: WorldLike a => a -> [Move] -> [Move]
select_good_moves _ [] = []
select_good_moves w (m:ms) = if good then m:rest else rest
            where good = case m of
                    GoUp -> can_move
                    GoDown -> can_move && not (isRock w up) -- can't kill us
                    GoRight -> can_move || isRock w dest && isEmpty w (destination dest GoRight) --FIXME test fo kill
                    GoLeft -> can_move || isRock w dest && isEmpty w (destination dest GoLeft) --FIXME test fo kill
                    Shave -> can_wait && razors w > 0 && any (\c -> getItemAt w c == Beard) (around place)
                    Wait -> can_wait
                    Abort -> True
                  can_wait = hasActiveEls && not (isEmpty w up && isRock w (destination up GoUp)) -- can't be killed
                  can_move = canMoveTo w dest
                  place = robotPosition w
                  dest = destination place m
                  up = destination place GoUp
                  rest = select_good_moves w ms
                  hasActiveEls = True -- not $ M.null $ M.filter (\i -> i == Rock True True || i == Rock False True) $ worldMap w

canMoveTo :: WorldLike wl => wl -> Coords -> Bool
canMoveTo w c = not $ case getItemAt w c of
                   Lift -> (collectedLambdas w /= allLambdas w)
                   Rock _ _ -> True
                   Wall -> True
                   Target _ -> True
                   Beard -> True
                   _ -> False

type SearchData w = (EstScore, (Score, (Path, (w, MoveResult))))
type SearchStream w = [SearchData w]

lifterBFS :: (WorldLikeHashable wl, WorldLikeEng wl) => ((Path, (wl, MoveResult)) -> EstScore) -> wl -> SearchStream wl
lifterBFS = lifterBFSDepth (-1)

-- A bunch of accessor functions
getMovesFromStream :: SearchData w -> Path
getMovesFromStream (_est_score, (_score, (rev_moves, _world_state))) = reverse rev_moves
getScoreFromStream :: SearchData w -> Score
getScoreFromStream (_est_score, (score, (_rev_moves, _world_state))) = score
getEstScoreFromStream :: SearchData w -> EstScore
getEstScoreFromStream (est_score, (_score, (_rev_moves, _world_state))) = est_score
getWorldStateFromStream :: SearchData w -> (w, MoveResult)
getWorldStateFromStream (_est_score, (_score, (_rev_moves, world_state))) = world_state

all_moves :: [Move]
all_moves = enumFrom $ toEnum 0
