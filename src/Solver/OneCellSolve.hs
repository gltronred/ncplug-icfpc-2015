module Solver.OneCellSolve ( oneCellSolver, isUnitsOneCell ) where

import Lib.Types
import Lib.Board
import Lib.BoardQuery
import Lib.Command
import Lib.Astar

import Data.IORef
import Data.Maybe
import System.Log.Logger

-- | Will solve correctly only if we have units from one cell
oneCellSolver :: SingleSolver
oneCellSolver limit powers board units = do
  -- TODO: Add assertion here
  state <- getNextTarget board $ centerUnit board $ head units
  ref <- newIORef state
  iterativeSolver limit "one" (oneCellOneStep ref) powers board units

-- | Checks assertion
isUnitsOneCell :: Board -> [GameUnit] -> Bool
isUnitsOneCell _ us = all ((==1) . length . gu_members) us

getNextTarget :: Board -> GameUnit -> IO State
getNextTarget board unit = do
  cell <- getCell unit
  let mtarget = Just $ head $ topRowEmptyCells board -- lowestReachableCell board
      mpath = pathAstar cell board $ fromJust mtarget
      wcell = moveCell MoveW cell
  case (mtarget,mpath) of
    (Just target,Just path) -> return $ State path $ Just target
    (Nothing, Just path) -> return $ State path $ Just wcell
    (Just target,Nothing) -> return $ State [wcell] $ Just target
    (Nothing,Nothing) -> return $ State [wcell] $ Just wcell

getCell :: GameUnit -> IO Cell
getCell u = if length (gu_members u) == 1
              then return $ head $ gu_members u
              else error "Something went wrong. Cannot work on units with more than one cell"

data State = State { getPath :: [Cell]       -- ^ path to target; if null and target not empty -- say W
                   , getTarget :: Maybe Cell -- ^ target; if target empty -- get new target
                   } deriving (Eq,Show,Read)

oneCellOneStep :: IORef State -> SolverCommander
oneCellOneStep ref start limit powerPhrases g@(GameState b h u us ls_old sc out) = do
  prevState@(State prevPath mprevTarget) <- readIORef ref
  if null prevPath && isJust mprevTarget
    then
    do
      writeIORef ref $ State [] Nothing
      return $ Just (Command MoveW Nothing)
    else
    do
      State path (Just target) <- if null prevPath
                                  then getNextTarget b u
                                  else return prevState
      noticeM "oneCellSolver" $ "Score: " ++ show sc
      noticeM "oneCellSolver" $ drawBoardEx b u [('o',path),('*',[target])]
      cell <- getCell u
      let result = commandFromTo cell (head path) >>= \x -> return (Command x Nothing)
          newState = State (tail path) $ Just target
      writeIORef ref newState
      return result

