module Solver.OneCellSolveEx ( oneCellSolverEx ) where

import Lib.Types
import Lib.Board
import Lib.BoardQuery
import Lib.Command
import Lib.Astar

import Debug.Trace
import Data.IORef
import Data.Maybe
import Data.List
import Data.Function

-- | Will solve correctly only if we have units from one cell
oneCellSolverEx :: SingleSolver
oneCellSolverEx limit powers board units = do
  -- TODO: Add assertion here
  state <- getNextTarget board $ centerUnit board $ head units
  ref <- newIORef state
  iterativeSolver limit "oneEx" (oneCellOneStep ref) powers board units

getNextTarget :: Board -> GameUnit -> IO State
getNextTarget board unit = do
  cell <- getCell unit
  cellCW <- getCell $ moveUnit RotCW unit
  let target = head $ bottomRowEmptyCells board -- TODO: change to top'
      mpathS = pathAstar cell board target
      mpathCW = fmap (cellCW:) $ pathAstar cellCW board target
      mps = map fromJust $ filter isJust [mpathCW, mpathS]
      mpath = if null mps then Nothing
              else Just $ minimumBy (compare `on` length) mps
  case mpath of
    Nothing -> do
      let mt = lowestReachableCell board
          mp = pathAstar cell board $ fromJust mt
      case (mt,mp) of
        (Nothing,_) -> return $ State [] (Cell 0 0) WrongMove
        (Just t,Nothing) -> return $ State [] t WrongMove
        (Just t,Just p) -> return $ State p t ValidMove
    Just path -> return $ State path target ValidMove

getCell :: GameUnit -> IO Cell
getCell u = return $ unitLowerPoint u

data State = State { getPath :: [Cell]       -- ^ path to target; if null and target not empty -- say W
                   , getTarget :: Cell       -- ^ target
                   , getResult :: MoveResult -- ^ result of previous move
                   } deriving (Eq,Show,Read)

oneCellOneStep :: IORef State -> SolverCommander
oneCellOneStep ref start limit powerPhrases g@(GameState b h u us ls_old sc out) = do
  prevState@(State _ _ prevRes) <- readIORef ref
  if prevRes == WrongMove
    then return Nothing
    else
    do
      State path target res <- case prevRes of
        ValidMove -> do
          return prevState
        Frozen -> do
          getNextTarget b u
        WrongMove -> do
          getNextTarget b u
      --putStrLn $ "Power phrases: " ++ show powerPhrases
      --putStrLn $ "Score: " ++ show sc
      --putStrLn $ drawBoardEx b u [('o',path),('*',[target])]
      cell <- getCell u
      let result = if null path
                        then Command MoveW Nothing
                        else case commandFromTo cell $ head path of
                          Nothing -> Command RotCW Nothing
                          Just s -> Command s Nothing
          newState = if null path
                     then State [] target Frozen
                     else State (tail path) target moveRes
          moveRes = commandValid g (command_code result)
      writeIORef ref newState
      return $ Just result

