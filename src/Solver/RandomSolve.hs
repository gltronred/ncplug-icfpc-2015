module Solver.RandomSolve ( randomSolver ) where

import Lib.Types
import Lib.Board
import Lib.BoardQuery
import Lib.Command

import System.Random

randomSolver :: SingleSolver
randomSolver limit powerPhrases board units = iterativeSolver limit "random" randomStep powerPhrases board units

randomStep :: SolverCommander
randomStep start limit powers (GameState b h u us ls_old sc out) = do
  action <- randomRIO (0,5)
  putStrLn $ drawBoard b u
  return $ Just $ map (\x -> Command x Nothing) [MoveE,MoveW,MoveSE,MoveSW,RotCW,RotCCW] !! action

