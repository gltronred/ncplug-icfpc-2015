module Solver.UnitAStarSolve ( unitAStarSolve ) where

import Lib.Types
import Lib.Board
import Lib.BoardQuery
import Lib.Command
import Lib.Astar

import Debug.Trace
import Data.IORef
import Data.Maybe
import Data.List
import Data.Function
import System.Log.Logger

-- | Will solve correctly only if we have units from one cell
unitAStarSolve :: SingleSolver
unitAStarSolve limit powers board units = do
  -- TODO: Add assertion here
  state <- getNextTarget board $ centerUnit board $ head units
  ref <- newIORef state
  iterativeSolver limit "unitA*" (oneUnitOneStep ref) powers board units

cellToUnit :: GameUnit -> Cell -> GameUnit
cellToUnit u@(GameUnit cs c@(Cell px py) r) (Cell x y) = GameUnit (map add cs) (add c) r
  where
    (Cell lx ly) = unitLowerPoint u
    dx = x - lx
    dy = y - ly
    add (Cell i j) = Cell (i + dx) (j + dy)

allEmptyCells :: Board -> [Cell]
allEmptyCells b = es
   where
     (PosY top) = topRow b
     (PosY btm) = bottomRow b
     es = concatMap ((flip emptyCellsIn) b) (map PosY [top..max btm (top - 2)])
  
getNextTarget :: Board -> GameUnit -> IO State
getNextTarget board unit = do
  let targets = filter (unitWithinBoard board) $ map (cellToUnit unit) $ allEmptyCells board
      mpaths = sortBy (compare `on` (gu_pivot.snd)) $ filter (isJust.fst) $ zip (map (pathUnitAstar unit board) targets) targets
  case mpaths of
    [] -> return $ State [] (GameUnit [] (Cell 0 0) 0) WrongMove
    (Just path, target):_ -> return $ State path target ValidMove

data State = State { getPath :: [GameUnit]   -- ^ path to target; if null and target not empty -- say W
                   , getTarget :: GameUnit   -- ^ target
                   , getResult :: MoveResult -- ^ result of previous move
                   } deriving (Eq,Show,Read)

oneUnitOneStep :: IORef State -> SolverCommander
oneUnitOneStep ref start limit powerPhrases g@(GameState b h u us ls_old sc out) = do
  prevState@(State _ _ prevRes) <- readIORef ref
  if prevRes == WrongMove
    then return Nothing
    else
    do
      State path target res <- case prevRes of
        ValidMove -> do
          return prevState
        Frozen -> do
          getNextTarget b u
        WrongMove -> do
          getNextTarget b u
      noticeM "solver.unitA*" $ "Power phrases: " ++ show powerPhrases
      noticeM "solver.unitA*" $ "Score: " ++ show sc
      noticeM "solver.unitA*" $ drawBoardEx b u [('|', map gu_pivot path),('*', gu_pivot target: gu_members target)]
      let result = if null path
                        then MoveW
                        else case commandFromToUnit u $ head path of
                          Nothing -> RotCW
                          Just s -> s
          newState = if null path
                     then State [] target Frozen
                     else State (tail path) target moveRes
          moveRes = commandValid g result
      writeIORef ref newState
      return $ Just (Command result Nothing)

