module Solver.TrajectorySolver where

-- solves puzzle unit by unit. Builds trajectory for unit moving it as far to the bottom as possible

import Lib.BFS
import Lib.Board
import Lib.BoardQuery
import Lib.Types
import Lib.Command

import Data.Maybe
import Debug.Trace
import Data.IORef
import System.Log.Logger
import Data.List(maximumBy)

data SearchState = SeS { sesUnit :: !GameUnit, sesCmds :: ![Command], sesDepth :: !Int} deriving Show
data SolverState = SoS { sosCmds :: [Command] } | SFixed

-- looks for a way to put unit as deep as possible
findGoodUnitTrajectories :: Board -> GameUnit -> [(EstScore, SearchState)]
findGoodUnitTrajectories board unit = bestFirstSearch sesUnit state_gen heu (SeS unit [] 0)
  where
    h = board_height board
    w = board_width board
    heu :: SearchState -> Int
    heu ss = let u = sesUnit ss
                 (b', kills, sc) = addUnitToBoard board u 0
             in maximum (map (rowFullness b' . PosY) [cell_y (unitUpperPoint u) .. cell_y (unitLowerPoint u)])
                 + cell_y (unitUpperPoint u) + sc + kills  -- - sesDepth ss `div` 5
    state_gen :: SearchState -> [SearchState]
    state_gen ss =
      let u = sesUnit ss
          fake_gs = fakeGameState board u --TODO HACK
          usrs' = map (\c -> (c,commandValid2 fake_gs c)) allCmdCodes
      in map (\(c,(Just u', _)) -> SeS u' (wrapCommand c : sesCmds ss) (sesDepth ss + 1)) $ filter ((==ValidMove) . snd . snd) usrs'

-- looks through N candidate solutions returning best
sieveBest :: GameState -> Int -> [(EstScore, SearchState)] -> IO SearchState
sieveBest gs depth (c:cs) = sieveBest' depth c cs
  where sieveBest' 0 best _ = do
          infoM "trajectory" $ "Selected; heu: " ++ show (fst best)
          infoM "trajectory" $ drawBoard (game_board gs) (sesUnit $ snd best)
          return $ snd best
        sieveBest' d b@(b_heu, b_ss) (c@(c_heu, c_ss):cs) = do
          debugM "trajectory" $ "Visited:\n" ++ drawBoard (game_board gs) (sesUnit c_ss)
          best' <- case isJust (freezingCmd (game_board gs) (sesUnit c_ss)) && c_heu > b_heu of
            True -> do infoM "trajectory" $ "New best; heu: " ++ show c_heu ++ "; depth=" ++ show d
                       infoM "trajectory" $ drawBoard (game_board gs) (sesUnit c_ss)
                       return c
            False -> return b
          sieveBest' (depth-1) best' cs
        sieveBest' d b [] = do
          infoM "trajectory" $ "No variants left; heu: " ++ show (fst b) ++ "; depth: " ++ show d
          infoM "trajectory" $ drawBoard (game_board gs) (sesUnit $ snd b)
          return $ snd b

trajectorySolver :: SingleSolver
trajectorySolver limit powers board units = do
  solver_state <- newIORef SFixed
  iterativeSolver limit "solver.trajectory" (solveUnit solver_state) powers board units

-- [String] -> GameState -> IO (Maybe Command)
solveUnit :: IORef SolverState -> SolverCommander
solveUnit ss start limit pws gs = do
  st <- readIORef ss
  case st of
    -- usual move
    SoS (c:cs) -> do writeIORef ss st {sosCmds = cs}
                     noticeM  "trajectory" $ " Score: " ++ show (game_scores gs) ++ ", units left: " ++ show (length $ game_units gs)
                     noticeM  "trajectory" $ drawBoard (game_board gs) (game_unit gs)
                     return $ Just c
    -- fixing move
    SoS [] -> do let fixing_move = fromMaybe (error "must be fixable") $ freezingCmd (game_board gs) (game_unit gs) 
                 writeIORef ss SFixed
                 return $ Just $ wrapCommand fixing_move
    -- calculating new trajectory
    SFixed -> do let candidate_trajs = findGoodUnitTrajectories (game_board gs) (game_unit gs)
                 best <- sieveBest gs 1000 candidate_trajs
                 writeIORef ss $ SoS (reverse $ sesCmds best)
                 solveUnit ss start limit pws gs
