module Solver.GreedySolver where

import Lib.Types
import Lib.BoardQuery
import Lib.Board
import Lib.Command
import Lib.Astar

import Data.Function
import Data.IORef
import Data.List
import Data.Maybe
import qualified Data.Set as Set
import System.Log.Logger

newtype Border = Border { unBorder :: [Cell] -- ^ From left to right
                        } deriving (Eq,Show,Read)

data Envelope = Envelope { envUnit :: GameUnit
                         , envW  :: Cell
                         , envE  :: Cell
                         } deriving (Eq,Show,Read)

data State = State { getBorder :: [Cell]
                   , getPath :: [GameUnit]
                   , getResult :: MoveResult
                   } deriving (Eq,Show,Read)

makeBorder :: Board -> Border
makeBorder = Border . getBoundaryCells

makeEnvelope :: GameUnit -> Envelope
makeEnvelope u@(GameUnit members (Cell px py) rot) = Envelope u w e
  where toW = cell_x
        xs = map toW members
        wX = maximum xs
        w = Cell wX py
        eX = minimum xs
        e = Cell eX py

updateBorder = undefined

deadPixelsAfter :: Board -> GameUnit -> Int
deadPixelsAfter board unit = deadPixels (frst $ addUnitToBoard board unit 0) - deadPixels board

diff (Cell x y) (Cell u v) = Cell (u-x) (v-y)
add (Cell x y) (Cell u v) = Cell (u+x) (v+y)

moveTo :: Cell -> GameUnit -> GameUnit
moveTo pos (GameUnit members pivot rot) = GameUnit (map (add pos) members) (add pos pivot) rot

generateAllPositions :: Board -> Border -> Envelope -> [Cell]
generateAllPositions board border (Envelope unit w e) = let
  allPositions = concatMap (\c -> [add w c, add e c, diff w c, diff e c]) $ unBorder border
  in filter (unitWithinBoard board . flip moveTo unit) allPositions

greedyTarget :: Board -> GameUnit -> IO State
greedyTarget b u = do
  let border = makeBorder b
      envelope = makeEnvelope u
      positions = generateAllPositions b border envelope
      addUnit b u = frst $ addUnitToBoard b u 0
      deadPixelsWith b u pos = deadPixels $ addUnit b $ moveTo pos u
      linesWith b u pos = scnd $ addUnitToBoard b (moveTo pos u) 0
      heu pos = 10 * deadPixelsWith b u pos - cell_y pos - 11 * linesWith b u pos
      sorted = sortBy (compare `on` heu) positions
      mpaths = filter isJust $ map (pathUnitAstar u b . flip moveTo u) sorted
  return $ case mpaths of
    [] -> State positions [] WrongMove
    (Just path:_)  -> State positions path ValidMove -- TODO: Change to border

greedySolverStep :: IORef State -> SolverCommander
greedySolverStep ref start limit powerPhrases g@(GameState b h u us ls_old sc out) = do
  prevState@(State _ _ prevRes) <- readIORef ref
  if prevRes == WrongMove
    then return Nothing
    else
    do
      State border path res <- case prevRes of
        ValidMove -> do
          return prevState
        Frozen -> do
          greedyTarget b u
        WrongMove -> do
          greedyTarget b u
      noticeM "solver.greedy" $ "Score: " ++ show sc
      noticeM "solver.greedy" $ drawBoardEx b u [('o', map gu_pivot path)]
      let Just result = if null path
                        then Just MoveW -- TODO: change to freezing command
                        else commandFromToUnit u $ head path
          newState = if null path
                     then State border [] Frozen
                     else State border (tail path) moveRes
          moveRes = commandValid g result
      writeIORef ref newState
      return $ Just $ Command result Nothing

greedySolver :: SingleSolver
greedySolver limit powers board units = do
  state <- greedyTarget board $ centerUnit board $ head units
  ref <- newIORef state
  iterativeSolver limit "greedy" (greedySolverStep ref) powers board units

