module Solver.TrajectorySolverWP (
    trajectorySolverWP
  ) where

{-
 - Solves puzzle unit by unit. Builds trajectory for unit moving it as far to the bottom as possible
 _   _       _
| \ | | ___ | |_ ___  ___
|  \| |/ _ \| __/ _ \/ __|
| |\  | (_) | ||  __/\__ \
|_| \_|\___/ \__\___||___/

 * Что-то не так с выводом, проверить механику

 * Поправить фингерпринт солвераWP!, учитывать количество сказанных слов

 * Научить солвер предпочитать более длинные слова более коротким

 - (обсудили, лучше искать эвристику)
   Сделать повторное использование траекторий, не считать всё по-новой для каждой фигуры.
   Например, ориентироваться на ближайший непустой ряд

 * Фильтровать невалидные слова из спеллбука заранее

 - (обсудили, неважно)
   Добавление слов силы сильно расширяет ветвление. Нужно как-то ограничить его,
   например, говорить меньше слов при уменьшении свободного пространства

 * Подумать про многопоточность

 * Сделать контроль времени выполнения

 * Подумать что-то про контроль памяти

--}

import Lib.BFS
import Lib.IO
import Lib.Board
import Lib.BoardQuery
import Lib.Types
import Lib.Command
import Lib.BB

import Control.Monad
import Data.IORef
import Data.Maybe
import System.Log.Logger
import Data.Array
import Data.Time.Clock

import Data.Set hiding (map,filter,foldl)
import qualified Data.Set as Set

data Action = AC Command | AW WordOfPower
    deriving(Show)

data SearchState = SeS {
      sesUnit :: !GameUnit
    , sesActions :: ![Action]
    , sesFixed :: !Bool
    , sesNWords :: !Int
    , sesWordLen :: !Int
    , sesHist :: History
    , sesWorked :: NominalDiffTime
    }

data SolverState = SoS {
      sosCmds :: [Command]
    , sosLimit :: STime
    }

-- Converts combined Actions back into commands
sesCmd :: SearchState -> [Command]
sesCmd = concat . map toCmd . reverse . sesActions where
    toCmd (AC c) = [c]
    toCmd (AW w) = w


-- Try to apply a list of commands, return cumulative result
commandListValid :: GameState -> [Command] -> (MoveResult, History, GameUnit)
commandListValid gs cmds = go where
    go = foldl (\(mr,h,u) c ->
                 let
                    (u',mr') = commandValid2 gs{game_history=h, game_unit=u} (command_code c)
                 in
                 case (mr == ValidMove, mr') of
                    (False, _)        -> (mr,h,u)
                    (True, ValidMove) -> (ValidMove, Set.insert (unitToHistUnit $ fromJust u') h, (fromJust u'))
                    (True, Frozen)    -> (Frozen, h, u)
                    (True, WrongMove) -> (WrongMove, h, u)
               )
               (ValidMove, game_history gs, game_unit gs)
               (cmds)



-- looks for a way to put unit as deep as possible
--
findGoodUnitTrajectories :: Board -> SpellBook -> GameState -> [(EstScore, SearchState)]
findGoodUnitTrajectories board spellbook gs =
    bestFirstSearch fingerprint state_gen heu (SeS unit [] False 0 0 (game_history gs) (toEnum 0))
  where
    board = game_board gs
    board_row_fills = listArray (0, board_height board) $ map (rowFullness board . PosY) [0..board_height board] -- memoizing row fills for heu
    board_empty_rows = length $ takeWhile (==0) $ Data.Array.elems board_row_fills

    unit = game_unit gs
    unit_bb = unitBB unit
    unit_size = max (py $ bbHeight unit_bb) (px $ bbWidth unit_bb)

    -- fingerprint ss = (sesUnit ss, (sesNWords ss)*(sesWordLen ss), sesFixed ss)
    -- fingerprint ss = sesUnit ss
    fingerprint ss = (sesUnit ss, sesFixed ss)

    -- heu :: SearchState -> Int
    -- heu ss = 2*(cell_y $ unitUpperPoint $ sesUnit ss) + (sesNWords ss)

    heu :: SearchState -> Int
    heu ss = let u = sesUnit ss
             in (2*(cell_y $ unitUpperPoint $ sesUnit ss) +
                      maximum (map (board_row_fills !) [cell_y (unitUpperPoint u) .. cell_y (unitLowerPoint u)])
                     ) +
                ((sesNWords ss))


    state_gen ss
        | sesFixed ss = []
        | otherwise = let
            u_bottom = cell_y $ unitLowerPoint $ sesUnit ss
            spells = map AW spellbook
            actions = if u_bottom > board_empty_rows - 2 then map AC allCommands ++ spells -- on lower part try anything
                      else (if even u_bottom then AC (wrapCommand MoveSW) : spells else AC (wrapCommand MoveSE) : spells)
            in catMaybes $ map (tryAction ss) actions

    -- Apply a single command to the state
    tryAction ss a@(AC cmd) =
        let
            u = sesUnit ss
            hs = sesHist ss
            (mbu', mr) = commandValid2 gs{game_history = hs, game_unit = u} (command_code cmd)
        in case mbu' of
            Just u' -> Just $ SeS u' (a:sesActions ss) (isFrozen mr) (sesNWords ss) (sesWordLen ss)
                                     (Set.insert (unitToHistUnit u') hs) (sesWorked ss)
            Nothing -> Nothing

    -- Apply the word of power to the state
    tryAction ss a@(AW cmds) =
        let
            u = sesUnit ss
            hs = sesHist ss
            (mr, h', u') = commandListValid gs{game_history = hs, game_unit = u} cmds
        in
        case (mr == ValidMove) of
            True -> Just $ SeS u' (a : sesActions ss) (isFrozen mr)
                                  ((sesNWords ss) + 1) ((sesWordLen ss) + (length cmds))
                                  h' (sesWorked ss)
            False -> Nothing


isLimitExceeded :: UTCTime -> STime -> IO (Bool, NominalDiffTime)
isLimitExceeded start (STime mlimit) = do
  current <- getCurrentTime
  let worked = current `diffUTCTime` start
  return $ case mlimit of
    Nothing -> (False, worked)
    Just tl -> (fromRational (toRational worked) > tl - 0.01, worked)



-- looks through N candidate solutions returning best
sieveBest :: Int -> UTCTime -> STime -> GameState -> [(EstScore, SearchState)] -> IO SearchState
sieveBest depth start limit@(STime mlimit) gs (c:cs) = sieveBest' depth c cs
    where
        sieveBest' depth b@(b_heu, b_ss) (c@(c_heu, c_ss):cs) = do
          (limitExceeded, worked) <- isLimitExceeded start limit
          if limitExceeded || isNothing mlimit && depth <= 0
            then
            do
              warningM "trajectory" $ "TIME LIMIT: worked " ++ show worked ++ "s"
              infoM "trajectory" $ "Selected; heu: " ++ show c_heu
              infoM "trajectory" $ drawBoard (game_board gs) (sesUnit c_ss)
              return c_ss {sesWorked = worked}
            else
            do
              debugM "trajectory" $ "Visited:\n" ++ drawBoard (game_board gs) (sesUnit c_ss)
              best' <- case sesFixed c_ss && c_heu >= b_heu of
                True -> do infoM "trajectory" $ "New best; heu: " ++ show c_heu ++ "; worked=" ++ show worked ++ "s"
                           infoM "trajectory" $ drawBoard (game_board gs) (sesUnit c_ss)
                           return c
                False -> return b
              sieveBest' (depth-1) best' cs
        sieveBest' depth (b_heu, b_ss) [] = do
          (_, worked) <- isLimitExceeded start limit
          infoM "trajectory" $ "No variants left; heu: " ++ show b_heu ++ "; worked: " ++ show worked ++ "s"
          infoM "trajectory" $ drawBoard (game_board gs) (sesUnit $ b_ss)
          return $ b_ss {sesWorked = worked}

-- -- looks through N candidate solutions returning best
-- sieveBest :: Int -> [(EstScore, SearchState)] -> SearchState
-- sieveBest depth (c:cs) = sieveBest' depth c cs
--   where sieveBest' 0 best _ = snd best
--         sieveBest' d b@(b_heu, b_ss) (c@(c_heu, c_ss):cs) =
--           let best' = if sesFixed c_ss && c_heu >= b_heu then c else b
--           in sieveBest' (depth-1) best' cs
--         sieveBest' _ b [] = snd b

trajectorySolverWP :: SingleSolver
trajectorySolverWP tLim@(STime limit) powers board units = do
  solver_state <- newIORef (SoS [] tLim)
  spellbook <- return (mkSpellBook powers)
  infoM "solver.twp" $ "Using following spells:"
  forM_ spellbook $ \cmds -> do
    infoM "solver.twp" $ (show cmds)
  let limitBest = STime $ Just $ fromMaybe 15 limit -- $ Just $ maybe 5 (/ fromIntegral (length units)) limit
  iterativeSolver tLim "trajectoryWP" (
        solveUnit limitBest {- FIXME: limitBest goes to nowhere! -}
                    spellbook solver_state) powers board units

nm = "trajectoryWP"

-- [String] -> GameState -> IO (Maybe Command)
solveUnit :: STime -> SpellBook -> IORef SolverState -> SolverCommander
solveUnit _ spellbook ss start limit pws gs = do
  (limitExceeded, worked) <- isLimitExceeded start limit
  case limitExceeded of {
    {-
     -
     - [10:14:53] Сергей Миронов: С архитектурой беда - походу, если из solveUnit
     - вернуть Nothing, прервется весь oneStep и выдаст солюшен. Это, наверное
     - правильно, но только внутри solveUnit глобальный таймаут (который я только
     - что добавил) нужно считать не по юнитовскому атймауту, а по таймауту для
     - одного файла
     -}
    True -> return Nothing ;
    False -> do

  st <- readIORef ss
  case sosCmds st of
    (c:cs) -> do
        writeIORef ss st {sosCmds = cs}
        debugM nm $ "Score: " ++ show (game_scores gs)
        -- debugM nm $ "Next: " ++ show (map command_code cs)
        debugM nm $ "Issuing: " ++ show (c)
        debugM nm $ drawBoard (game_board gs) (game_unit gs)
        return $ Just c
    [] -> do
        let candidate_trajs = findGoodUnitTrajectories (game_board gs) spellbook gs
        now <- getCurrentTime
        let (STime mbLimitRest) = sosLimit st
            limitBest = case mbLimitRest of
                            Just lr -> let forUnit = lr / 3
                                       in STime $ Just $ forUnit
                            Nothing -> STime Nothing
        best <- sieveBest 5000 now limitBest gs candidate_trajs
        let worked = sesWorked best
            rest = case mbLimitRest of
                        Just limitRest -> STime $ Just $ limitRest - realToFrac worked
                        Nothing -> STime Nothing
        warningM "solver.twp" $ "Limits: " ++ show limit ++ "; " ++ show limitBest ++ "; " ++ show rest
        noticeM nm $ "Score: " ++ show (game_scores gs)
        noticeM nm $ drawBoard (game_board gs) (game_unit gs)
        writeIORef ss $ SoS (sesCmd best) rest
        noticeM nm $ "Words expected: " ++ (show $ sesNWords best)
        solveUnit rest spellbook ss start limit pws gs
  ; }

