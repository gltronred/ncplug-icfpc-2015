module Lib.Command where

import Control.Monad
import Data.List
import Data.Maybe
import Data.Time
import Debug.Trace
import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Time.Clock
import System.Log.Logger

import Lib.Board
import Lib.BoardQuery
import Lib.IO
import Lib.Scores
import Lib.Types

-- Checks if command @cmd is valid against history and the board in a given game state.
-- Returns a MoveResult.
commandValid :: GameState -> CommandCode -> MoveResult
commandValid gs c= snd $ commandValid2 gs c

commandValid2 :: GameState -> CommandCode -> (Maybe GameUnit, MoveResult)
commandValid2 (GameState b h u us ls_old sc _) cmd =
    let
      u' = moveUnit cmd u
    in
      case (not $ Set.member (unitToHistUnit u') h, unitWithinBoard b u') of
        (True,  True)  -> (Just u', ValidMove)
        (False, True)  -> (Nothing, WrongMove)
        (True,  False) -> (Just u, Frozen)
        (False, False) -> (Nothing, WrongMove)


type SolverCommander = UTCTime -> STime -> [String] -> GameState -> IO (Maybe Command)

-- | Set unit's position to @p (matching pivot)
unitMoveToPivot :: Cell -> GameUnit -> GameUnit
unitMoveToPivot p@(Cell tx ty) u@(GameUnit mm pv r) =
    let
        Cell px py = gu_pivot u
        (dx,dy) = (tx - px, ty - py)
    in
        GameUnit (map (offsetCell (dx,dy)) (gu_members u)) (offsetCell (dx,dy) (gu_pivot u)) r

centerUnit :: Board -> GameUnit -> GameUnit
centerUnit (Board w h _) (GameUnit members pivot rot) = GameUnit (map mv members) (mv pivot) rot
  where mv (Cell x y) = Cell (x-minX + delta) y
        delta = (w - uw)`div`2
        minX = minimum xs
        uw = maximum xs - minX + 1
        xs = map cell_x members

isValidPos :: Board -> GameUnit -> Bool
isValidPos (Board _ _ f) (GameUnit members _ _) = let
  memberSet = Set.fromList members
  in Set.null $ Set.intersection memberSet f

-- whether unit can be frozen in one turn
unitCanBeFrozen :: Board -> GameUnit -> Bool
unitCanBeFrozen b u = let
  us' = map (moveUnit `flip` u) allCmdCodes
  in any (not . unitWithinBoard b) us'

-- returns a cmd which freezez unit, if any
freezingCmd :: Board -> GameUnit -> Maybe CommandCode
freezingCmd b u = listToMaybe $ filter (\c -> not $ unitWithinBoard b (moveUnit c u)) allCmdCodes

oneStep :: UTCTime -> STime -> SolverCommander -> [String] -> GameState -> IO (Either ([Command], Score) GameState)
oneStep timeStart timeFor c ss g@(GameState b h u us ls_old scores out) = do
  cmd <- c timeStart timeFor ss g
  case cmd of
    Nothing -> return $ Left (out, scores)
    Just cmd'@(Command cc _) ->
      case commandValid g cc of
        WrongMove -> do
            errorM "oneStep" $ "WRONG MOVE: " ++ show cmd'
            return $ Left (out, 0)
        Frozen    ->
          case us of
            [] -> return $ Left (cmd':out, scores)
            _  -> let newUnit = centerUnit b $ head us
                      (newBoard, ls, scores') = addUnitToBoard b u ls_old
                      newScores = scores + scores'
                  in do
                    let finish = not $ isValidPos newBoard newUnit
                    when finish $ do
                        errorM "oneStep" $ "FINISH"
                    return $ if finish
                              then Left (cmd' : out, newScores)
                              else Right $ GameState newBoard (initHistory newUnit) newUnit (tail us) ls newScores (cmd':out)
        ValidMove -> do let u' = moveUnit cc u
                        return $ Right $ GameState b (Set.insert (unitToHistUnit u') h) u' us ls_old scores (cmd':out)

iterativeSolver :: STime -> String -> SolverCommander -> [String] -> Board -> [GameUnit] -> IO Solution
iterativeSolver timeForSeed name c ss b us = do
  time_start <- getCurrentTime
  (out, score) <- iterator (oneStep time_start timeForSeed c ss) initialState
  let sol = encode_commands ss $ reverse out
      pow = power_scores ss sol
  time_stop <- getCurrentTime
  return $ Solution { s_tag = name ++ "; at " ++ show time_start,
                      s_cmds = sol,
                      s_score = score + pow,
                      s_elapsed = diffUTCTime time_stop time_start
                    }
  where
    -- FIX: add here isValidPos check
    unit0 = centerUnit b $ head us
    initialState = GameState b (initHistory unit0) unit0 (tail us) 0 0 []

iterator :: (a -> IO (Either r a)) -> a -> IO r
iterator f s = do
  s' <- f s
  case s' of
    Right s'' -> iterator f s''
    Left r -> return r

