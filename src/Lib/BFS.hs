{-# LANGUAGE FlexibleContexts, ExistentialQuantification, ScopedTypeVariables #-}
module Lib.BFS where

import qualified Data.Heap as H
import Data.List
import qualified Data.Set as S

type Score = Int -- Precise score, for selecting the best solution
type EstScore = Int -- Estimated score, for guiding the search
type Hash = Int

-- walks the search space, generates all possible moves in best first order
bestFirstSearch :: forall s fp. (Ord fp) => (s -> fp) -> (s -> [s]) -> (s -> EstScore) -> s -> [(EstScore, s)]
bestFirstSearch fingerprinter move_generator evaluator start_state =
      bsf S.empty (H.fromAscList [(evaluator start_state, start_state)] :: H.MaxPrioHeap EstScore s)
  where bsf visited_states solutions =
          case H.view solutions of
            Nothing -> []
            Just (best, rest) -> let
              cur_st = snd best
              finger = fingerprinter cur_st
              visited_states' = S.insert finger visited_states
              child_states = move_generator cur_st
              child_scores = map evaluator child_states
              solutions' = foldl' (flip H.insert) rest $ zip child_scores child_states
             in case S.notMember finger visited_states of
                  True -> best : bsf visited_states' solutions' -- output new state and take children into account
                  False -> bsf visited_states rest -- discarding computation, make use of laziness
