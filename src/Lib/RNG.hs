module Lib.RNG -- (next, mask)
       where

import Data.Bits
import Data.Word

multiplier = 1103515245 :: Word32
increment = 12345 :: Word32

mask = ((1`shiftL`15)-1) `shiftL` 16 :: Word32

next :: Word32 -> (Word32, Word32)
next seed = let seed' = multiplier * seed + increment
            in (seed', (seed .&. mask) `shiftR` 16)

