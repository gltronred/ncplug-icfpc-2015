module Lib.IO where

import Lib.Types
import Lib.JSON
import Lib.Scores

import Control.Arrow
import Data.Char (toLower)
import Data.Function
import Data.List
import qualified Data.Map.Strict as Map
import Data.Maybe
import System.Random


commands0 = "R1 O0 P1 Q1 P1 O0 N0 N0 P1 R1 Q1 P1 O0 P1 Q1 R1 P1 N0 N0 Q1 S1 N1 T1 S1 R1 P1 R1 Q1 P1 O0 O0 P1 Q1 R1 P1 N0 N0"

problem0 = "{\"height\":10,\
            \ \"width\":10,\
            \ \"sourceSeeds\":[0],\
            \ \"units\":[{\"members\":[{\"x\":0,\"y\":0}],\
            \             \"pivot\":{\"x\":0,\"y\":0}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":2,\"y\":0}],\
            \             \"pivot\":{\"x\":1,\"y\":0}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":0,\"y\":2}],\
            \             \"pivot\":{\"x\":0,\"y\":1}},\
            \            {\"members\":[{\"x\":2,\"y\":0},{\"x\":0,\"y\":1},{\"x\":2,\"y\":2}],\
            \             \"pivot\":{\"x\":1,\"y\":1}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":1,\"y\":1},{\"x\":0,\"y\":2}],\
            \             \"pivot\":{\"x\":0,\"y\":1}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":1,\"y\":0}],\
            \             \"pivot\":{\"x\":0,\"y\":0}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":1,\"y\":0}],\
            \             \"pivot\":{\"x\":1,\"y\":0}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":0,\"y\":1}],\
            \             \"pivot\":{\"x\":0,\"y\":0}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":0,\"y\":1}],\
            \             \"pivot\":{\"x\":0,\"y\":1}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":1,\"y\":0},{\"x\":2,\"y\":0}],\
            \             \"pivot\":{\"x\":0,\"y\":0}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":1,\"y\":0},{\"x\":2,\"y\":0}],\
            \             \"pivot\":{\"x\":1,\"y\":0}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":1,\"y\":0},{\"x\":2,\"y\":0}],\
            \             \"pivot\":{\"x\":2,\"y\":0}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":0,\"y\":1},{\"x\":0,\"y\":2}],\
            \             \"pivot\":{\"x\":0,\"y\":0}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":0,\"y\":1},{\"x\":0,\"y\":2}],\
            \             \"pivot\":{\"x\":0,\"y\":1}},\
            \            {\"members\":[{\"x\":0,\"y\":0},{\"x\":0,\"y\":1},{\"x\":0,\"y\":2}],\
            \             \"pivot\":{\"x\":0,\"y\":2}},\
            \            {\"members\":[{\"x\":1,\"y\":0},{\"x\":0,\"y\":1},{\"x\":1,\"y\":2}],\
            \             \"pivot\":{\"x\":1,\"y\":0}},\
            \            {\"members\":[{\"x\":1,\"y\":0},{\"x\":0,\"y\":1},{\"x\":1,\"y\":2}],\
            \             \"pivot\":{\"x\":1,\"y\":1}},\
            \            {\"members\":[{\"x\":1,\"y\":0},{\"x\":0,\"y\":1},{\"x\":1,\"y\":2}],\
            \             \"pivot\":{\"x\":1,\"y\":2}}],\
            \ \"id\":0,\
            \ \"filled\":[],\
            \ \"sourceLength\":100}"


commands_chars :: Map.Map CommandCode [Char]
commands_chars = Map.fromList [(MoveW,  "p'!.03")
                              ,(MoveE,  "bcefy2")
                              ,(MoveSW, "aghij4")
                              ,(MoveSE, "lmno 5")
                              ,(RotCW,  "dqrvz1")
                              ,(RotCCW, "kstuwx")]

deserialize_command :: Char -> Maybe Command
deserialize_command c | elem c "p'!.03" = mk MoveW
                      | elem c "bcefy2" = mk MoveE
                      | elem c "aghij4" = mk MoveSW
                      | elem c "lmno 5" = mk MoveSE
                      | elem c "dqrvz1" = mk RotCW
                      | elem c "kstuwx" = mk RotCCW
                      | otherwise       = Nothing
                      where
                        mk cc = Just $ Command cc (Just c)

decode_commands :: [Char] -> Maybe [Command]
decode_commands = sequence . map (deserialize_command . toLower)


-- Create words Of power (filter incomplete commands)
mkSpellBook :: [String] -> SpellBook
mkSpellBook [] = []
mkSpellBook (w:ws) = case decode_commands w of
                        Just c -> c:(mkSpellBook ws)
                        Nothing -> mkSpellBook ws

--
-- FIXME: Old encode_commands appears to be broken!
--
-- encode_commands :: [String] -> [Command] -> [Char]
-- encode_commands powerPhrases commands = let
--   phraseCmds = map (id &&& map command_code . fromJust . decode_commands) powerPhrases
--   charFor c = head $ commands_chars Map.! c

--   encode [] = []
--   encode cmds@((Command c Nothing):other) =
--     case find (\(p,c) -> isPrefixOf c (map command_code cmds)) phraseCmds of
--         Just (phrase, _) -> phrase ++ encode (drop (length phrase) cmds)
--         Nothing -> charFor c : encode other
--   encode cmds@((Command c (Just s)):other) = s : (encode other)

--   in encode commands

encode_commands :: [String] -> [Command] -> [Char]
encode_commands _ cmds = reverse $ snd $ foldl' subst (rngGen, []) cmds where
    subst (g, cs) (Command _ (Just c)) = (g, c:cs)
    subst (g, cs) (Command cc Nothing) = let (i, g') = randomR (0, maxind) g
                                             c = (commands_chars Map.! cc) !! i in (g', c:cs)
    maxind = 5 -- 6 possible chars per command counting from 0
    rngGen = mkStdGen 395048 -- random seed from my head :)

--encode_commands :: [String] -> [Command] -> [Char]
--encode_commands [] commands = let charFor c = head $ commands_chars Map.! c in map charFor commands
--encode_commands powerPhrases commands = let
--  decPoPs = map (fromJust . decode_commands) powerPhrases
--  --phraseCmds = zip powerPhrases decPoPs
--  phraseCmds = zip decPoPs powerPhrases
--  bestPoP = maximumBy (morePowerIn commands) decPoPs
--  bestPoPChars = zip bestPoP $ fromJust $ lookup bestPoP phraseCmds
--  charFor c = head $ commands_chars Map.! c
--  encode [] = []
--  encode (c:cs) | c `elem` bestPoP = (fromJust $ lookup c bestPoPChars) : encode cs
--                | otherwise = charFor c : encode cs
--  in encode commands

-- Output commands -> Phrase-of-Power -> Phrase-of-Power -> Ordering
morePowerIn :: [Command] -> [Command] -> [Command] -> Ordering
morePowerIn commands = (compare `on` power)
  where
    power pop = power_score (length pop) (repetitions pop)
    repetitions pop = countReps pop commands

countReps :: Eq a => [a] -> [a] -> Int
countReps = countReps' 0
  where
    countReps' acc _ [] = acc
    countReps' acc pat lst = if isPrefixOf pat lst
                              then countReps' (acc+1) pat $ drop l lst
                              else countReps' acc pat $ tail lst
      where
        l = length pat

