module Lib.BB where

import Lib.Types
import Lib.Board
import Lib.BoardQuery
import Lib.Command

import Data.List
import Control.Monad
import Control.Monad.State
import Data.Char
import Data.Maybe
import Data.Set as Set hiding (foldl,map)

-- Bounding Box
data BB = BB {
      wi_min_x :: PosX
    , wi_max_x :: PosX
    , wi_min_y :: PosY
    , wi_max_y :: PosY
} deriving(Show)

bbWidth :: BB -> PosX
bbWidth (BB (PosX xi) (PosX xm) yi ym) =  PosX $ xm - xi

bbHeight :: BB -> PosY
bbHeight (BB xi xm (PosY yi) (PosY ym)) =  PosY $ ym - yi


unitBB :: GameUnit -> BB
unitBB u = BB (PosX xmin) (PosX xmax) (PosY ymin) (PosY ymax) where
    Cell _ ymin = unitLowerPoint u
    Cell _ ymax = unitUpperPoint u
    Cell xmin _ = unitLeftmostPoint u
    Cell xmax _ = unitRightmostPoint u

bbUnion :: BB -> BB -> BB
bbUnion (BB xi xm yi ym) (BB xi2 xm2 yi2 ym2) = BB (min xi xi2) (max xm xm2) (min yi yi2) (max yi yi2)

wordBBforUnit :: GameUnit -> WordOfPower -> BB
wordBBforUnit u wp = bb where
    (bb, unit') =
        let unit = unitMoveToPivot (Cell 0 0) u
        in flip execState (unitBB unit,unit) $ do
            forM_ wp $ \(Command cc _) -> do
                (bb,u) <- get
                put ((bbUnion (unitBB $ moveUnit cc u) bb), moveUnit cc u)

