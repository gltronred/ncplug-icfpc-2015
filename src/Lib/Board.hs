module Lib.Board where

import Lib.Types
import Lib.IO
import Lib.BoardQuery
import Lib.Scores

import Debug.Trace
import Text.Printf
import Control.Monad
import Control.Monad.Writer
import Data.Maybe
import Data.List

import Data.Set hiding (map,filter,foldl)
import qualified Data.Set as Set

import Data.Map hiding (map,filter,foldl)
import qualified Data.Map as Map


frst (x,y,z) = x
scnd (x,y,z) = y
thrd (x,y,z) = z


--
--  In case of the following unit (in square coordinates)
--
--   ....x
--   x.x..
--   .x...
--
--  we should get [Just 1,Just 0,Just 1,Nothing,Just 2]
--
unitBottomSurface :: GameUnit -> [Maybe Int]
unitBottomSurface u =
    let
        Cell lx ly = unitLowerPoint u

        comp (Cell x1 y1) (Cell x2 y2) =
            case x1 `compare` x2 of
                EQ -> y2 `compare` y1
                o -> o
        x_ordered = map (\(Cell x y) -> (x,ly - y))
            ( nubBy (\a b -> cell_x a == cell_x b) $ sortBy comp $ gu_members u)

        filled_holes = (Just $ snd $ head x_ordered) : (
                            concat $ map (\((xn,yn),(x,y)) ->
                                            (replicate (xn-x-1) Nothing) ++ [Just yn]
                                         ) ((tail x_ordered)`zip`x_ordered))
    in
        filled_holes


unitWithinBoard :: Board -> GameUnit -> Bool
unitWithinBoard b = and . map (cellWithinBoard b) . gu_members

addRot r d = (r + d) `mod` 6

moveUnit :: CommandCode -> GameUnit -> GameUnit
moveUnit RotCW  (GameUnit ms p r) = GameUnit (map (rotateCell RotCW p) ms) p $ addRot r 1
moveUnit RotCCW (GameUnit ms p r) = GameUnit (map (rotateCell RotCCW p) ms) p $ addRot r (-1)
moveUnit m (GameUnit ms p r) = GameUnit (map (moveCell m) ms) (moveCell m p) r

-- | Return (newUnit, isFixed newUnit)
moveAndCheckUnit :: CommandCode -> Board -> History -> GameUnit -> Maybe (GameUnit,Bool)
moveAndCheckUnit cmd b h u =
    let
        u' = moveUnit cmd u
    in
        case unitToHistUnit u' `Set.member` h of
            True -> Nothing
            False -> Just (u', not $ unitWithinBoard b u')

moveCell :: CommandCode -> Cell -> Cell
moveCell MoveW (Cell x y) = Cell (x - 1) y
moveCell MoveE (Cell x y) = Cell (x + 1) y
moveCell MoveSW (Cell x y) = Cell (x + y `mod` 2 - 1) (y + 1)
moveCell MoveSE (Cell x y) = Cell (x + y `mod` 2) (y + 1)
moveCell RotCW c = c
moveCell RotCCW c = c

offsetCell :: (Int,Int) -> Cell -> Cell
offsetCell (dx,dy) (Cell x y) = Cell (x+dx) (y+dy)

cellTo3d (Cell c r) = (x, y, z)
 where x = c - (r - r `mod` 2) `div` 2
       z = r
       y = -x-z

cellFrom3d (x,y,z) = Cell (x + (z - z `mod` 2) `div` 2) z

rotateCell :: CommandCode -> Cell -> Cell -> Cell
rotateCell dir pivot cell =
  let p_3d@(p_i, p_j, p_k) = cellTo3d pivot
      c_3d@(c_i, c_j, c_k) = cellTo3d cell
      (i,j,k) = (c_i - p_i, c_j - p_j, c_k - p_k)
  in case dir of
    RotCW -> cellFrom3d (-k + p_i, -i + p_j, -j + p_k)
    RotCCW -> cellFrom3d (-j + p_i, -k + p_j, -i + p_k)

-- Draws a board @b, unit @u and the set of additional figures @sym. A figure
-- is list of cells, annotated with brush char
drawBoardEx :: Board -> GameUnit -> [(Char,[Cell])] -> String
drawBoardEx b u sym =
    let
        w = board_width b
        h = board_height b
        f = board_filled b
        margin = 4
        for = flip map
    in
    execWriter $ do

        tell $ map (const ' ') [0..margin-1]
        forM_ [0..(w-1)] $ \x -> do
            tell (printf " %d" $ x`mod`10)

        tell "\n"

        forM_ [0..(h-1)] $ \y -> do
            tell (printf ("%"++(show margin)++"d ") y)

            when ((y `mod` (2::Int)) /= 0) $ do
                tell " "

            forM_ [0..w-1] $ \x -> do
                let c = Cell x y
                let p = (if Set.member c f then do
                            Just 'F'
                        else if elem c (gu_members u) then
                            (if c == gu_pivot u then
                                Just 'U'
                             else
                                Just 'u'
                            )
                        else if c == gu_pivot u then
                            Just 'p'
                        else
                            Nothing
                     )

                let ps = listToMaybe $ execWriter $ do
                            forM_ sym $ \(symbol,lst) ->
                                if c`elem`lst then
                                    tell [symbol]
                                else
                                    return ()

                case (p,ps) of
                    (Just x,Nothing) -> tell (x:" ")
                    (Nothing,Just x) -> tell (x:" ")
                    (Nothing,Nothing) ->
                        if (x`mod`2 == 0) then tell ". " else tell ", "
                    (Just x,Just y) -> tell (x:y:"")

            tell "\n"
        return ()

drawBoard :: Board -> GameUnit -> String
drawBoard b u = drawBoardEx b u []

-- Fix unit @u in board @b.
-- Clear rows and move down as needed
-- TODO: calculate scores too
addUnitToBoard :: Board -> GameUnit -> Int -> (Board, Int, Int)
addUnitToBoard b u ls_old = (b', ls, scores)
  where
    (b', ls) = clearRowsFrom 0 (h-1) $ fixUnit b u
    scores = move_score u ls ls_old
    fixUnit b u = let filled = board_filled b
                  in b { board_filled = Set.union filled (Set.fromList $ gu_members u)}
    clearRowsFrom ls (-1) b = (b, ls)
    clearRowsFrom ls y b | rowIsFull b (PosY y) = clearRowsFrom (ls+1) y $ shiftRowsDownFrom y b
                         | otherwise = clearRowsFrom ls (y-1) b
    shiftRowsDownFrom from b = let filled' = Set.map moveDown $ Set.filter (\(Cell _ y) -> y /= from) $ board_filled b
                                   moveDown c@(Cell x y) = if y < from
                                                           then Cell x (y+1)
                                                           else c
                               in b {board_filled = filled'}
    h = board_height b

-- | Command to get from one cell to its neighbor cell
commandFromTo :: Cell -> Cell -> Maybe CommandCode
commandFromTo from to = find (\cmd -> moveCell cmd from == to) [MoveE,MoveW,MoveSW,MoveSE,RotCW]

-- | Command to get from one cell to its neighbor cell
commandFromToUnit :: GameUnit -> GameUnit -> Maybe CommandCode
commandFromToUnit from to = find (\cmd -> moveUnit cmd from == to) [MoveE,MoveW,MoveSW,MoveSE,RotCW,RotCCW]

allMovesRot = [MoveW, MoveE, MoveSE, MoveSW, RotCW, RotCCW]
allMoves = [MoveW, MoveE, MoveSE, MoveSW]

-- Return the set of non-occupied non-fixed moves, from to the GameUnit given
aStarUnitNeighbours :: Board -> GameUnit -> Set GameUnit
aStarUnitNeighbours b u = Set.fromList $ filter (unitWithinBoard b) allNei
  where
    allNei = zipWith ($) (map (moveUnit) (if length (gu_members u) == 1 then allMoves else allMovesRot)) (repeat u)

-- See BoardLevel
data BoardLevelMap = BoardLevelMap {
      bl_posy :: PosY
    , bl_level :: Map PosX PosY
    } deriving(Show)


-- Builds the heights map for points taken at level @ly@
-- for every x, @BoardLevelMap@ contains coordinates of the non-empty cell along the
-- bar.
boardLevel :: Board -> PosY -> BoardLevelMap
boardLevel b ly = BoardLevelMap ly bl where
    h = board_height b
    w = board_width b
    empty_board_level = Map.fromList (map (\x -> (PosX x, PosY h)) [0..w-1])

    bl = Set.foldl (\bl (Cell x y) ->
                     if (PosY y) < ly then
                        bl
                     else
                        Map.adjust (\ly' ->
                                      if (PosY y) < ly' then
                                          (PosY y)
                                      else
                                          ly'
                                   ) (PosX x) bl
                   ) empty_board_level (board_filled b)

