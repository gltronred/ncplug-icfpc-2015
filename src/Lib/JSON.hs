module Lib.JSON where

import Lib.Types

import Data.Aeson
import Data.Aeson.Types

instance FromJSON Input where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = drop 6 }
instance ToJSON Input where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = drop 6 }

instance FromJSON Cell where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = drop 5 }
instance ToJSON Cell where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = drop 5 }

instance FromJSON Unit where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = drop 5 }
instance ToJSON Unit where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = drop 5 }

instance FromJSON Output where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = drop 7 }
instance ToJSON Output where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = drop 7 }


