module Lib.Scores where

import Lib.Types

import Data.List (foldl')
import Data.Text (breakOnAll, pack)


-- Return the number of non-overlapping occurrences of sub in str.
countSubStrs str sub = length $ breakOnAll (pack sub) (pack str)


move_score :: GameUnit -> Int -> Int -> Int
move_score unit ls ls_old = points + line_bonus
  where
    size = length $ gu_members unit
    points = size + 100 * (1 + ls) * ls `div` 2
    line_bonus  = if ls_old > 1
                  then floor ((fromIntegral ls_old - 1) * fromIntegral points / 10)
                  else 0

power_score :: Int -> Int -> Int
power_score len reps = 2 * len * reps + power_bonus
  where
    power_bonus = if reps > 0
                  then 300
                  else 0

power_scores :: [String] -> String -> Int
power_scores pops cmds = foldl' f 0 pops
  where
    f acc pop = acc + power_score (length pop) (countSubStrs cmds pop)

-- points = move_score + power_scores
