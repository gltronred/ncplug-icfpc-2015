module Lib.BoardQuery where

import Lib.Types

import Control.Monad

import Debug.Trace
import Data.Maybe
import Data.Foldable (foldl', foldl)
import Data.Graph.AStar

import Data.List hiding (foldl', foldl)
import qualified Data.List as List

import Data.Set hiding (map, foldl, foldl', empty)
import qualified Data.Set as Set

import Data.Map hiding (map, foldl, foldl', empty)
import qualified Data.Map as Map

import Prelude hiding (map, foldl, foldl')
import qualified Prelude as Prelude


-- Compare cells
xL (Cell x1 y1) (Cell x2 y2) =
    case x1 `compare` x2 of
        EQ -> y1 `compare` y2
        x -> x

xG (Cell x1 y1) (Cell x2 y2) = (Cell x2 y1) `yL` (Cell x1 y2)

yL (Cell x1 y1) (Cell x2 y2) =
    case y1 `compare` y2 of
        EQ -> x1 `compare` x2
        x -> x

-- Compare cells (Y-swapped)
yG (Cell x1 y1) (Cell x2 y2) = (Cell x1 y2) `yL` (Cell x2 y1)

-- | Returns the (max-Y) point of GameUnit @u
unitLowerPoint :: GameUnit -> Cell
unitLowerPoint = head . sortBy yG . gu_members

-- | Returns the (min-Y) point of GameUnit @u
unitUpperPoint :: GameUnit -> Cell
unitUpperPoint = head . sortBy yL . gu_members

unitLeftmostPoint :: GameUnit -> Cell
unitLeftmostPoint = head . sortBy xL . gu_members

unitRightmostPoint :: GameUnit -> Cell
unitRightmostPoint = head . sortBy xG . gu_members

-- Checks if the cell is on board
-- FIXME: function also checks for fixed cells, despite the name
cellWithinBoard :: Board -> Cell -> Bool
cellWithinBoard b c@(Cell x y) =
    let
        w = board_width b
        h = board_height b
        filled = board_filled b
    in
        x >= 0 && x < w &&
        y >= 0 && y < h &&
        Set.notMember c filled

-- Return the set of non-occupied non-fixed cells, adjucent to the Cell given
cellNeighbours :: Board -> Cell -> Set Cell
cellNeighbours b (Cell x y) =
    let
        allnei =
            if y`mod`2 == 0 then
                map (\(x,y) -> Cell x y) [(x-1,y), (x+1,y), (x,y-1),
                                          (x,y+1), (x-1,y-1),
                                          (x-1,y+1)]
            else
                map (\(x,y) -> Cell x y) [(x-1,y), (x+1,y), (x,y-1),
                                          (x,y+1), (x+1,y+1), (x+1,y-1)
                                         ]
    in
        Set.fromList $ Prelude.filter (cellWithinBoard b) allnei


-- Return the set of non-occupied non-fixed cells, adjucent to the Cell given
aStarNeighbours :: Board -> Cell -> Set Cell
aStarNeighbours b (Cell x y) =
    let
        allnei =
            if y`mod`2 == 0 then
                map (\(x,y) -> Cell x y) [(x-1,y), (x+1,y), -- (x,y-1),
                                          (x,y+1), -- (x-1,y-1),
                                          (x-1,y+1)]
            else
                map (\(x,y) -> Cell x y) [(x-1,y), (x+1,y), -- (x,y-1),
                                          (x,y+1), (x+1,y+1)-- , (x+1,y-1)
                                         ]
    in
        Set.fromList $ Prelude.filter (cellWithinBoard b) allnei

-- Return the set of non-occupied non-fixed cells below given Cell
lowerNeighbours :: Board -> Cell -> Set Cell
lowerNeighbours b (Cell x y) =
    let
        allnei =
            if y`mod`2 == 0 then
                map (\(x,y) -> Cell x y) [(x,y+1), (x-1,y+1)]
            else
                map (\(x,y) -> Cell x y) [(x,y+1), (x+1,y+1)]
    in
        Set.fromList $ Prelude.filter (cellWithinBoard b) allnei

unitNeighbours :: Board -> GameUnit -> Set Cell
unitNeighbours b u = Set.unions (map (cellNeighbours b) $ gu_members u)

-- Reformulate (board_filled @b) into the Map form
boardMap :: Board -> Map Cell (Maybe Cell)
boardMap b = filledMap where
    w = board_width b
    h = board_height b
    ps = [ Cell x y | x <- [0..w-1], y  <- [0..h-1] ]
    emptyMap = Map.fromList (ps `zip`(repeat Nothing))
    filledMap = foldl (\acc c -> Map.insert c (Just c) acc) emptyMap (board_filled b)

reverseMap :: Map Cell (Maybe Cell) -> Set Cell
reverseMap = foldl (\acc c -> case c of {Just c -> Set.insert c acc; Nothing -> acc ; }) Set.empty

-- | Returns a row of fixed cells on board at (Pos @y)
boardFixedRow :: Board -> PosY -> [Cell]
boardFixedRow b (PosY y) =
    let
        bm = boardMap b
        w = board_width b
        h = board_height b
    in
        catMaybes $ map (\(PosX x) -> join $ Map.lookup (Cell x y) bm) $ rangeX (PosX 0) (maxX b)

rowIsFull :: Board -> PosY -> Bool
rowIsFull b y = rowFullness b y == board_width b

rowFullness :: Board -> PosY -> Int
rowFullness b (PosY y) = let
     bm = boardMap b
  in Map.size (Map.filterWithKey (\(Cell _ y1) v -> y1 == y && v /= Nothing) bm)

-- | Return a row, nearest to the GameUnit @u, matching @pred
--
-- FIXME UNCHECKED
nearestMatchingRow :: ([Cell] -> Bool) -> GameUnit -> Board -> Maybe (PosY, [Cell])
nearestMatchingRow pred u b =
    let
        h = board_height b
        Cell ux uy = unitLowerPoint u
        fs = sortBy yG $ Set.toList $ board_filled b
    in
        foldl (\acc (PosY y) ->
            case acc of
                Nothing ->
                    let
                        row = boardFixedRow b (PosY y)
                    in
                        if (pred row) then Just (PosY y, row) else Nothing
                x@(Just _) -> x
            ) Nothing (rangeY (PosY uy) (maxY b))

-- See this StackOverflow answer
-- http://stackoverflow.com/a/1266822/1133157
squareCoords :: Cell -> (Float,Float)
squareCoords (Cell x y) = (fx,fy) where
    fx = (fromIntegral x) - (fromIntegral y)/2.0
    fy = (fromIntegral y) + fx

-- FIXME: float, sqrt, .. inefficient? Who cares for now?
cellDist :: Cell -> Cell -> Float
cellDist c1 c2 =
    let
        (fx1,fy1) = squareCoords c1
        (fx2,fy2) = squareCoords c2
    in
        {- We should not overestimate the distance -}
        ( sqrt((fx1-fx2)^2 + (fy1-fy2)^2) )

-- | Returns empty cells in top not empty row
topRowEmptyCells :: Board -> [Cell]
topRowEmptyCells b = emptyCellsIn (topRow b) b

-- | Returns empty cells in bottom not empty row
bottomRowEmptyCells :: Board -> [Cell]
bottomRowEmptyCells b = case lowestReachableCell b of
  Nothing -> emptyCellsIn (bottomRow b) b
  Just (Cell x y) -> emptyCellsIn (PosY y) b

-- | Returns index of top not empty row
-- Returns last row if board is empty
topRow :: Board -> PosY
topRow (Board _ h filled) = PosY $ Set.foldl' (\minY (Cell _ y) -> min minY y) (h-1) filled

-- | Returns index of bottom not empty row
-- Returns last row if board is empty
bottomRow :: Board -> PosY
bottomRow (Board _ h filled) = PosY $ Set.foldl' (\minY (Cell _ y) -> max minY y) (h-1) filled

-- | Returns list of empty cells in row
emptyCellsIn :: PosY -> Board -> [Cell]
emptyCellsIn (PosY y) (Board w h filled) = if y < 0 || y >= h then [] else
  let allCells = Set.fromList [Cell x y | x<-[0..w-1]]
      rowCells = Set.filter (\(Cell _ y') -> y==y') filled
  in Set.toAscList $ Set.difference allCells rowCells


-- | Returns lowest reachable (by 1-cell unit) cell on board @b
-- Nothing means that top row is full
lowestReachableCell :: Board -> Maybe Cell
lowestReachableCell b@(Board _ h _) = listToMaybe $ findLowest $ emptyCellsIn (PosY 0) b
  where
    findLowest empties | Prelude.null nextEmpties || h <= (cell_y.head) nextEmpties = empties
                       | otherwise = findLowest nextEmpties
      where
        nextEmpties = Set.toList $ foldl' (\acc c -> Set.union acc $ lowerNeighbours b c) Set.empty empties

-- | Returns a number of dead cells on board
deadPixels :: Board -> Int
deadPixels b@(Board _ _ filled) = Set.size $ Set.difference (board_filled $ fillDeadPixels b) filled

-- | Returns a board with reachable cells only
fillDeadPixels :: Board -> Board
fillDeadPixels b@(Board w h filled) = let
  reachable = Set.empty
  border = Set.singleton $ Cell (w`div`2) 0
  add :: Set Cell -> Set Cell -> (Set Cell, Set Cell)
  add reached border = (reached' , Set.foldl' addNeighbor Set.empty border `Set.difference` reached')
    where reached' = Set.union reached border
  addNeighbor :: Set Cell -> Cell -> Set Cell
  addNeighbor border cell = Set.union border $ aStarNeighbours b cell
  bfs :: Set Cell -> Set Cell -> Set Cell
  bfs reached border | Set.null border = reached
                     | otherwise = uncurry bfs $ add reached border
  all = Set.fromList [Cell x y | x<-[0..w-1], y<-[0..h-1]]
  in b { board_filled = all `Set.difference` bfs reachable border }

-- | Returns a list of boundary cells (without cells on top of board)
getBoundaryCells :: Board -> [Cell]
getBoundaryCells board@(Board w h filled) = let
  noDead = fillDeadPixels board
  border = Set.unions [ Set.fromList [Cell x h    | x <- [-1..w]]
                      , Set.fromList [Cell (-1) y | y <- [-1..h]]
                      , Set.fromList [Cell w y    | y <- [-1..h]]
                      ]
  in Set.toAscList $ Set.union border $ Set.filter (not . Set.null . cellNeighbours noDead) filled


