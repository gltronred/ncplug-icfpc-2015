{-# LANGUAGE DeriveGeneric #-}

module Lib.Types where

import GHC.Generics

import Control.Concurrent.ParallelIO.Local (Pool)
import Data.Set (Set(..))
import qualified Data.Set as S
import Data.Word
import Data.List(sort)
import Data.Time(NominalDiffTime,UTCTime)

type WordOfPower = [Command]

type SpellBook = [WordOfPower]

data Board = Board { board_width :: Int
                   , board_height :: Int
                   , board_filled :: Set Cell
                   } deriving(Show,Eq,Read,Generic)

data Input = Input { input_id :: Int
                   , input_units :: [Unit]
                   , input_width :: Int
                   , input_height :: Int
                   , input_filled :: [Cell]
                   , input_sourceLength :: Int
                   , input_sourceSeeds :: [Word32]
                   } deriving (Eq,Show,Read,Generic)

data Cell = Cell { cell_x :: Int
                 , cell_y :: Int
                 } deriving (Eq,Show,Read,Generic, Ord)

data Unit = Unit { unit_members :: [Cell]
                 , unit_pivot :: Cell
                 } deriving (Eq,Show,Read,Generic, Ord)

data Output = Output { output_problemId :: Int
                     , output_seed :: Word32
                     , output_tag :: String
                     , output_solution :: String -- in reverse order
                     } deriving (Eq,Show,Read,Generic)

data CommandCode = MoveW
                 | MoveE
                 | MoveSW
                 | MoveSE
                 | RotCW
                 | RotCCW
                 deriving (Eq,Ord,Show,Read,Enum,Bounded)

data Command = Command { command_code :: CommandCode, command_char ::  (Maybe Char) }
    deriving(Show,Read,Eq)

-- Some solvers may know exactly how to encode the command
-- type CommandEx = (Command, Maybe Char)

wrapCommand :: CommandCode -> Command
wrapCommand cc = Command cc Nothing

allCommands :: [Command]
allCommands = map wrapCommand allCmdCodes

allCmdCodes :: [CommandCode]
allCmdCodes = [minBound..maxBound]

allMoveCommands :: [Command]
allMoveCommands = filter (\(Command x _) -> x /= RotCW && x /= RotCCW) allCommands


type History = Set HistUnit
initHistory :: GameUnit -> History
initHistory = S.singleton . unitToHistUnit

data GameUnit = GameUnit { gu_members :: ![Cell]
                         , gu_pivot :: !Cell
                         , gu_rotations :: !Int
                         } deriving (Show,Read)

-- a compact representation of unit for history needs, cells reordered
newtype HistUnit = HistUnit { hu_pivot_members :: [Cell] } deriving (Show,Eq,Ord)

unitToHistUnit u = HistUnit $ gu_pivot u : sort (gu_members u)

instance Ord GameUnit where
  compare (GameUnit ms1 p1 _) (GameUnit ms2 p2 _) = compare (Unit ms1 p1) (Unit ms2 p2)

instance Eq GameUnit where
  (==) (GameUnit ms1 p1 _) (GameUnit ms2 p2 _) = (==) (Unit ms1 p1) (Unit ms2 p2)

type Score = Int

data GameState = GameState { game_board :: Board
                           , game_history :: History
                           , game_unit :: GameUnit
                           , game_units :: [GameUnit]
                           , game_prev_lines_removed :: Int
                           , game_scores :: Score
                           , game_output :: [Command]
                           } deriving (Eq,Show)

-- a mock object for using as adapter
fakeGameState board unit = GameState { game_board = board
                                     , game_history = initHistory unit
                                     , game_unit = unit
                                     , game_units = []
                                     , game_prev_lines_removed = 0
                                     , game_scores = 0
                                     , game_output = []
                                     }

data MoveResult = ValidMove
                | Frozen
                | WrongMove
                 deriving (Eq,Show,Read,Ord)

isFrozen Frozen = True
isFrozen _      = False

-- solution along with stats
data Solution = Solution { s_tag :: String
                         , s_cmds :: String
                         , s_score :: Score
                         , s_elapsed :: NominalDiffTime
                         } deriving Show

newtype STime = STime { getTime :: Maybe Double
                      } deriving (Eq,Show,Read)

type SingleSolver = STime -> [String] -> Board -> [GameUnit] -> IO Solution
type Solver = Pool -> STime -> [String] -> Input -> IO [(Output, Solution)]

type Submitter = [Output] -> IO() -- routing for displaying or sending results

type SolverTable = [(String, Solver)] -- a catalogue of solvers available
type SubmitterTable = [(String, Submitter)] -- a catalogue of submitters available

inputToBoard :: Input -> Board
inputToBoard input = Board (input_width input) (input_height input) (S.fromList $ input_filled input)

u2gu :: Unit -> GameUnit
u2gu (Unit ms p) = GameUnit ms p 0

newtype PosX = PosX {px :: Int}
    deriving(Show,Ord,Eq)

newtype PosY = PosY { py :: Int}
    deriving(Show,Ord,Eq)

maxX b = PosX ((board_width b) - 1)
maxY b = PosY ((board_height b) - 1)

rangeY (PosY y1) (PosY y2) = map PosY [y1 .. y2]
rangeX (PosX x1) (PosX x2) = map PosX [x1 .. x2]

toCell :: PosX -> PosY -> Cell
toCell (PosX x) (PosY y) = Cell x y

