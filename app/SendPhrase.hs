module Main where

import Lib.Types
import Lib.JSON
import Submit

import Control.Monad
import Data.Aeson
import qualified Data.ByteString.Lazy as LBS
import Data.Char
import System.Environment
import System.Exit

main = do
  args <- getArgs
  when (length args < 2) $ putStrLn "Usage: send-phrase <problem-file> <phrase> [<phrase>...]" >> exitFailure
  let problemFile : phrases' = args
      phrases = map (map toLower) phrases'
  Just input <- fmap decode $ LBS.readFile problemFile
  let problemId = input_id input
      seeds = input_sourceSeeds input
  forM_ (zip [1..] phrases) $ \(idx, phrase) -> do
    putStrLn $ show idx ++ ": " ++ phrase ++ " (" ++ show (length phrase) ++ ")"
    send $ [Output problemId seed ("ph" ++ show idx) phrase | seed <- seeds]

