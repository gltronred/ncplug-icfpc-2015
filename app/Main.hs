module Main where

import Helper
import Lib.Types(SolverTable, output_solution)

import Submit
import Solver.ManuallySolve
import Solver.OneCellSolve
import Solver.OneCellSolveEx
import Solver.RandomSolve
import Solver.UnitAStarSolve
import Solver.TrajectorySolver
import Solver.TrajectorySolverWP
import Solver.GreedySolver

import Data.Char
import Data.Aeson as Aeson
import qualified Data.ByteString.Lazy.Char8 as BS

main = mkMain solvers submitters

-- Set trajectorySolverWP since it is the only solver which use words
bestSolver = trajectorySolverWP

-- bestSolver = (isUnitsOneCell, oneCellSolver) `combineWith` trajectorySolverWP
-- (isUnitsOneCell, oneCellSolver) `combineWith` randomSolver

solvers :: SolverTable
solvers = [ ("test",   mkSolver testSolver)
          , ("manual", mkSolver solveManually)
          , ("auto",   mkSolver bestSolver)  -- This is default solver. Should be our best solver
          , ("one",    mkSolver oneCellSolver)
          , ("oneEx",  mkSolver oneCellSolverEx)
          , ("random", mkSolver randomSolver)
          , ("unitAStar",  mkSolver unitAStarSolve)
          , ("trajectory", mkSolver trajectorySolver)
          , ("trajectoryWP", mkSolver trajectorySolverWP)
          , ("greedy", mkSolver greedySolver)
          ]

print_solution outputs = do
  putStrLn "solutions (uppercased): "
  mapM_ (putStrLn . map toUpper . output_solution) outputs

submitters = [ ("print", BS.putStrLn . Aeson.encode)
             , ("null", const (return ()))
             , ("print_solution", print_solution) -- prints solution in format which can be replayed by pasting to manual sover
             , ("submit", send)
             ]
