module Helper where

import Lib.RNG as RNG
import Lib.Types
import Lib.JSON

import Control.Concurrent.ParallelIO.Local
import Control.Monad
import Data.Aeson
import Data.ByteString.Lazy as LBS (readFile)
import Data.Char
import Data.List
import qualified Data.Set as S
import Data.Traversable (Traversable)
import System.Environment
import Data.Maybe(fromMaybe)
import Data.Word
import Data.Maybe
import System.Log.Logger
import System.IO(hPutStrLn, stderr)

data Main = Main { files :: [FilePath]
                 , timeLimit :: Maybe Int
                 , memoryLimit :: Maybe Int
                 , phrases :: [String]
                 , cores :: Maybe Int
                 , chosenSolver :: String
                 , chosenSubmitter :: String
                 , debugLevel :: Maybe Priority
                 , showHelp :: Bool
                 } deriving (Eq,Show,Read)

parseArgs :: [String] -> Main
parseArgs = foldl' addFlag (Main [] Nothing Nothing [] Nothing "auto" "submit" Nothing False) . by2
  where by2 [] = []
        by2 (x:y:ys) = (x,Just y) : by2 ys
        by2 (x:[]) = [(x,Nothing)]
        addFlag :: Main -> (String,Maybe String) -> Main
        addFlag m (flag,value') =
            let value = fromJust value'
            in case flag of
              "-f" -> m {files = value : files m}
              "-t" -> m { timeLimit = Just $ read value }
              "-m" -> m { memoryLimit = Just $ read value }
              "-p" -> m { phrases = map toLower value : phrases m }
              "-c" -> m { cores = Just $ read value }
              "-s" -> m { chosenSolver = value }
              "-S" -> m { chosenSubmitter = value }
              "-d" -> m { debugLevel = Just $ read value }
              "-h" -> m { showHelp = True }
              "--help" -> m { showHelp = True }
              _ -> m


testSolver :: SingleSolver
testSolver limit powerPhrases board units = return $ Solution { s_tag = "test", s_cmds = "ei!", s_score = -1, s_elapsed = 0}

genUnits :: Input -> Word32 -> [GameUnit]
genUnits input seed = let
  iterate2 fun seed = let (seed',res) = fun seed in res : iterate2 fun seed'
  unitLen = length $ input_units input
  getUnit = (input_units input !!)  . (`mod` unitLen) . fromIntegral
  in map u2gu $ take (input_sourceLength input) $ map getUnit $ iterate2 RNG.next seed


forConcurrently :: Pool -> [a] -> (a -> IO b) -> IO [b]
forConcurrently group inputs action = parallelInterleaved group $ map action inputs

-- solver takes a list of phrases of power, a board, a list of units that it should land
-- and returns output with any id and any seed
mkSolver :: SingleSolver -> Solver
mkSolver solver taskGroup timeForInputCore powerPhrases input = let
  timeForSeed = STime $ case timeForInputCore of
    STime Nothing -> Nothing
    STime (Just time) -> Just $ time / fromIntegral (length $ input_sourceSeeds input)
  in forConcurrently taskGroup (input_sourceSeeds input) $ \seed -> do
    let board = inputToBoard input
        units = genUnits input seed
    sol <- solver timeForSeed powerPhrases board units
    noticeM "mkSolver" $ "Score: " ++ show (s_score sol) ++ ";\tElapsed: " ++ show (s_elapsed sol)
    return ( Output { output_problemId = input_id input
                    , output_seed = seed
                    , output_tag = s_tag sol
                    , output_solution = s_cmds sol
                    },
             sol)


printHelp = do
  putStrLn $ "Usage: solution [-t SEC] [-m MB] [-c CORES] [-p STRING] [-S SUBMITTER] [-s SOLVER] [-d (DEBUG|NOTICE|INFO..)]"
  putStrLn $ "  The default sover is trajectoryWP"

-- solve takes a list of phrases of power and an input and returns output
-- sender takes a list of outputs and should do something
mkMain :: SolverTable -> SubmitterTable -> IO ()
mkMain solvers submitters = getArgs >>= \args -> let opts = parseArgs args in if (showHelp opts) then printHelp else do
  updateGlobalLogger rootLoggerName $ case debugLevel opts of
    Just level -> setLevel level
    Nothing -> removeHandler
  -- print opts
  let solver = fromMaybe (error "no solvers") $ lookup (chosenSolver opts) solvers `mplus`
                 (Just $ snd $ head $ solvers) -- default solver
  let submitter = fromMaybe (error "no submitters") $ lookup (chosenSubmitter opts) submitters `mplus`
                    (Just $ snd $ head $ submitters) -- default submitter
  debugM "mkMain" $ show $ chosenSolver opts
  debugM "mkMain" $ chosenSubmitter opts
  let poolSize = maybe 2 (+1) $ cores opts
  let timeForInputCore = case timeLimit opts of
        Nothing -> Nothing
        Just tl -> Just $ (fromIntegral tl-1) / fromIntegral (length $ files opts) * fromIntegral (poolSize-1)
  withPool poolSize $ \taskGroup -> do
    res <- fmap concat $ forConcurrently taskGroup (files opts) $ \f -> do
      json <- LBS.readFile f
      infoM "mkMain" $ "Loading " ++ f
      let einput = eitherDecode json
          Right input = einput
      solver taskGroup (STime timeForInputCore) (phrases opts) input
    let elapsed = sum $ map (s_elapsed . snd) res
        tasks = length res
        score = (sum $ map (s_score . snd) res) `div` tasks
    hPutStrLn stderr "\n ***************"
    hPutStrLn stderr $ "Task solved: " ++ show tasks
    hPutStrLn stderr $ "TotalScore average: " ++ show score ++ ";\tElapsed: " ++ show elapsed
    submitter $ map fst res


checkAllSeeds :: (Board -> [GameUnit] -> Bool) -> Input -> Bool
checkAllSeeds cond input = forAll (input_sourceSeeds input) $ cond (inputToBoard input). genUnits input
  where forAll = flip all

-- | Combines two solvers. Uses first if condition met and second otherwise
combineWith :: (Board -> [GameUnit] -> Bool, SingleSolver) -> SingleSolver -> SingleSolver
combineWith (cond, solver1) solver2 limit powerPhrases board units =
  if cond board units
  then solver1 limit powerPhrases board units
  else solver2 limit powerPhrases board units

