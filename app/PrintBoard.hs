-- module PrintBoard where

import Lib.Board
import Lib.BoardQuery
import Lib.JSON
import Lib.Types
import Lib.Command

import Data.List
import Control.Monad
import Data.Aeson
import qualified Data.ByteString.Lazy as LBS
import System.Environment
import Data.Set as Set hiding (foldl)

main = do
  files <- getArgs
  forM_ files $ \file -> LBS.readFile file >>= \json -> do
    let Just input = decode json
        board = inputToBoard input

        unit_figures = ['0'..'9'] `zip`
            (reverse $ foldl
                (\figs (offset, unit) -> unit : figs)
                []
                ([0..]`zip`(input_units input))
            )

    let smallBoard = (Board 6 6 (Set.empty))
    forM_ unit_figures $ \ (c,u) -> do
        putStrLn $ "Bottom surface: " ++ show  (unitBottomSurface $ u2gu u)
        putStrLn $ drawBoard  smallBoard (centerUnit smallBoard (u2gu u))

    putStrLn $ drawBoard board (GameUnit [] (Cell 0 0) 0)

