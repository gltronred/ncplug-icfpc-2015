-- module PrintWord where

import Lib.Board
import Lib.BoardQuery
import Lib.JSON
import Lib.Types
import Lib.Command
import Lib.IO
import Lib.BB

import Data.List
import Control.Monad
import Control.Monad.State
import Data.Aeson
import Data.Char
import qualified Data.ByteString.Lazy as LBS
import System.Environment
import Data.Set as Set hiding (foldl,map)

main = do
  words <- getArgs
  forM_ words $ \w -> do
    let nounit = GameUnit [Cell (-1) (-1)] (Cell (-1) (-1)) 0
    let unit = GameUnit [Cell 0 0] (Cell 0 0) 0

    let c = map deserialize_command (map toLower w)

    (good, trace, unit') <- return $
        flip execState (True,[],unit) $ do
            forM_ c $ \c -> do
                (b,trace,u) <- get
                case (b,c) of
                    (True, Just (Command cc _)) -> do
                        put (True, (gu_pivot (moveUnit cc u):trace), moveUnit cc u)
                    _ -> do
                        put (False, trace, u)

    let Cell xmax _ = maximumBy (\(Cell x y) (Cell x2 _)-> x`compare`x2) trace
    let Cell xmin _ = minimumBy (\(Cell x y) (Cell x2 _)-> x`compare`x2) trace
    let Cell _ ymax = maximumBy (\(Cell x y) (Cell _ y2)-> y`compare`y2) trace

    let width = ((xmax-xmin) + 8)

    let board = (Board width (ymax + 4) (Set.empty))
    let trace' = map (\(Cell x y) -> Cell (x + ((width`div`2) - ((xmax - xmin)`div`2))) (y+2)) trace

    putStrLn $ "Word of power: " ++ w
    putStrLn $ "Transcribed: " ++ (show $ map (>>=return . command_code) c)
    putStrLn $ drawBoardEx board nounit [('W',trace')]
    when (not good) $ do
        putStrLn $ "Word contains invalid character " ++ (show $ w !! (length trace)) ++ "!"
    return ()

