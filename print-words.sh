#!/bin/sh

./dist/build/print-word/print-word \
    "shub niggurath" \
    "hastur" \
    "ghatanothoa"\
    "yog sothoth"\
    "nithon"\
    "nyarlathotep"\
    "abdul alhazred"\
    "necronomicon"\
    "ei!" \
    "yuggoth" \
    "r'lyeh"\
    "ph'nglui mglw'nafh cthulhu r'lyeh wgah'nagl fhtagn!"
